# efeds_speccomp

![Versions](https://img.shields.io/badge/python->3.7-blue)
[![Documentation Status](https://readthedocs.org/projects/sdss-efeds_speccomp/badge/?version=latest)](https://sdss-efeds_speccomp.readthedocs.io/en/latest/?badge=latest)
[![Travis (.org)](https://img.shields.io/travis/sdss/efeds_speccomp)](https://travis-ci.org/sdss/efeds_speccomp)
[![codecov](https://codecov.io/gh/sdss/efeds_speccomp/branch/main/graph/badge.svg)](https://codecov.io/gh/sdss/efeds_speccomp)

Package to consolidate and compile spectroscopy in the eFEDS field
