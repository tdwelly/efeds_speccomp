
efeds_speccomp's documentation
=============================================

This is the documentation for the SDSS Python product efeds_speccomp. The current version is |efeds_speccomp_version|. You can install the package by doing

.. code-block:: console

  $ pip install sdss-efeds_speccomp


Contents
--------

.. toctree::
  :maxdepth: 2

  What's new in efeds_speccomp? <CHANGELOG>
  Introduction to efeds_speccomp <intro>


Reference
---------

.. toctree::
   :maxdepth: 1

   api


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
