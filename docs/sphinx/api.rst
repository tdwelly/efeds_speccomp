
.. _api:

efeds_speccomp Reference
=========================

.. _api-main:

Main
----

.. automodule:: efeds_speccomp.main
   :members:
   :undoc-members:
   :show-inheritance:
