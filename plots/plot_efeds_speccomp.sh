#!/usr/bin/env sh

cd ~/efeds_speccomp
mkdir -p plots

#do_prep="true"
do_prep="false"

if [ "$do_prep" = "true" ]; then
    ftcopy '~/eFEDS/SDSS_DR16/plates-dr16_eFEDS.fits[1][col *,subset_legacy(L)=(PROGRAMNAME=="legacy"?1:0); subset_boss(L)=(PROGRAMNAME=="boss"?1:0); subset_segue(L)=(PROGRAMNAME=="boss"||PROGRAMNAME=="legacy"?0:1)]' ./plots/plates-dr16_eFEDS.fits clobber=true mode=q

    ln -s ~/eFEDS/SDSS_DR16/specObj-dr16_eFEDS.fits ./plots/

    gawk '$1~/\)/ {flag=0} flag==1 {print $0} $1~/\(/{flag=1}'  ~/eFEDS/footprint/Teng_c946_final_DetMsk.ctr > plots/xray_outline.txt

    mangle-poly2poly -q -p+10 -ol1000  ~/eFEDS/bounds/super-trim-efeds.ply  - | gawk '$0!~/NaN|#/{print $1,$2}' > plots/super-trim-efeds.txt

    ftcopy "~/eFEDS/spec_obs/v5_13_1/platelist.fits[1][PLATE>=12525&&PLATE<=12548]" plots/plates-Mar2020.fits clobber=yes copyall=no mode=q

    ##ftcopy "~/eFEDS/spectra_compilation/spectra_compilation_eFEDS_${VERSION}.fits[1][ISNULL(strstr(ORIGIN,'sdss'))]" plotsdata/spectra_compilation_eFEDS_v0.6.1b_nonsdss.fits clobber=yes mode=q

    ln -s ~/eFEDS/spec_obs/v5_13_1/spZbest-merged_with_targeting_info.fits plots/spZbest-merged_with_targeting_info.fits

    ln -s ~/SDSSV/sas/sdss5/bhm/boss/spectro/redux/v6_0_2/eFEDS/spAll-v6_0_2-eFEDS.fits plots/spAll-v6_0_2-eFEDS.fits
    ln -s ~/SDSSV/sas/sdss5/bhm/boss/spectro/redux/v6_0_4/eFEDS/spAll-v6_0_4.fits plots/spAll-v6_0_4-eFEDS.fits

    ln -s ~/SDSSV/sas/sdss5/bhm/boss/spectro/redux/v6_0_4/conflist.fits plots/conflist_v6_0_4.fits
    ln -s ~/SDSSV/sas/sdss5/bhm/boss/spectro/redux/v6_0_2/conflist.fits plots/conflist_v6_0_2.fits
fi


VERSION="v1.4.3"
#############

stilts plot2sky \
   xpix=1800 ypix=900 \
   crowd=1.7 gridcolor=black gridaa=true texttype=antialias fontsize=20 \
   clon=135.98 clat=1.46 radius=5.538 \
   legend=true legpos=0.9958,0.019  \
   ra=1.49 ellipse=filled_ellipse translevel=1.9 \
   layer_1=Mark \
      in_1=plots/super-trim-efeds.txt ifmt_1=ASCII \
      lon_1=col1 lat_1=col2 \
      shading_1=auto size_1=3 \
   layer_2=Mark \
      in_2=plots/xray_outline.txt ifmt_2=ASCII \
      lon_2=col1 lat_2=col2 \
      shading_2=auto shape_2=filled_diamond color_2=blue \
   layer_3=SkyEllipse \
      in_3=plots/plates-dr16_eFEDS.fits icmd_3='select subset_legacy' \
      lon_3=RACEN lat_3=DECCEN \
      shading_3=translucent color_3=green \
      leglabel_3='SDSS legacy plates' \
   layer_4=SkyEllipse \
      in_4=plots/plates-dr16_eFEDS.fits icmd_4='select subset_boss' \
      lon_4=RACEN lat_4=DECCEN \
      shading_4=translucent color_4=blue \
      leglabel_4='BOSS plates' \
   layer_5=SkyEllipse \
      in_5=plots/plates-dr16_eFEDS.fits icmd_5='select subset_segue' \
      lon_5=RACEN lat_5=DECCEN \
      shading_5=translucent \
      leglabel_5='SEGUE plates' \
   layer_6=Mark \
      in_6=plots/specObj-dr16_eFEDS.fits \
      lon_6=PLUG_RA lat_6=PLUG_DEC \
      shading_6=auto shape_6=cross color_6=green \
      leglabel_6='SDSS DR16 spectra' \
   legseq=_3,_4,_5,_6  \
   out=plots/eFEDS_SDSS_map_plates_DR16_spectra.png



stilts plot2sky \
   xpix=1800 ypix=900 \
   crowd=1.7 gridcolor=black gridaa=true texttype=antialias fontsize=20 \
   clon=135.98 clat=1.46 radius=5.538 \
   legend=true legpos=0.9958,0.019 \
   ra=1.49 \
   layer_1=Mark \
      in_1=plots/super-trim-efeds.txt ifmt_1=ASCII \
      lon_1=col1 lat_1=col2 \
      shading_1=auto size_1=3 \
   layer_2=Mark \
      in_2=plots/xray_outline.txt ifmt_2=ASCII \
      lon_2=col1 lat_2=col2 \
      shading_2=auto shape_2=filled_diamond color_2=blue \
   layer_3=SkyEllipse \
      in_3=plots/plates-dr16_eFEDS.fits icmd_3='select subset_legacy' \
      lon_3=RACEN lat_3=DECCEN \
      shading_3=flat color_3=green \
   layer_4=SkyEllipse \
      in_4=plots/plates-dr16_eFEDS.fits icmd_4='select subset_boss' \
      lon_4=RACEN lat_4=DECCEN \
      shading_4=flat color_4=blue \
   layer_5=SkyEllipse \
      in_5=plots/plates-dr16_eFEDS.fits icmd_5='select subset_segue' \
      lon_5=RACEN lat_5=DECCEN \
      shading_5=flat \
   layer_6=Mark \
      in_6=plots/specObj-dr16_eFEDS.fits \
      lon_6=PLUG_RA lat_6=PLUG_DEC \
      shading_6=auto shape_6=cross color_6=green \
      leglabel_6='SDSS DR16 spectra' \
   layer_7=SkyEllipse \
      in_7=plots/plates-Mar2020.fits \
      lon_7=RACEN lat_7=DECCEN \
      shading_7=translucent ellipse_7=filled_ellipse color_7=orange translevel_7=0.83 \
      leglabel_7='SDSS-IV/eFEDS plates' \
   layer_8=Mark \
      in_8=plots/spZbest-merged_with_targeting_info.fits icmd_8='select IS_TILED' \
      lon_8=PLUG_RA lat_8=PLUG_DEC \
      shading_8=auto color_8=ff6633 \
      leglabel_8='SDSS-IV/eFEDS spectra' \
   legseq=_6,_7,_8 \
   out=plots/eFEDS_SDSS_map_plates_DR16_Mar2020_spectra.png



stilts plot2sky \
   xpix=1800 ypix=900 \
   crowd=1.7 gridcolor=black gridaa=true texttype=antialias fontsize=20 \
   clon=135.98 clat=1.46 radius=5.538 \
   legend=true legpos=0.9958,0.019 \
   ra=1.49 \
   layer_01=Mark \
      in_01=plots/super-trim-efeds.txt ifmt_01=ASCII \
      lon_01=col1 lat_01=col2 \
      shading_01=auto size_01=3 \
   layer_02=Mark \
      in_02=plots/xray_outline.txt ifmt_02=ASCII \
      lon_02=col1 lat_02=col2 \
      shading_02=auto shape_02=filled_diamond color_02=blue \
   layer_03=SkyEllipse \
      in_03=plots/plates-dr16_eFEDS.fits icmd_03='select subset_legacy' \
      lon_03=RACEN lat_03=DECCEN \
      shading_03=flat color_03=green \
   layer_04=SkyEllipse \
      in_04=plots/plates-dr16_eFEDS.fits icmd_04='select subset_boss' \
      lon_04=RACEN lat_04=DECCEN \
      shading_04=flat color_04=blue \
   layer_05=SkyEllipse \
      in_05=plots/plates-dr16_eFEDS.fits icmd_05='select subset_segue' \
      lon_05=RACEN lat_05=DECCEN \
      shading_05=flat \
   layer_06=Mark \
      in_06=plots/specObj-dr16_eFEDS.fits \
      lon_06=PLUG_RA lat_06=PLUG_DEC \
      shading_06=auto shape_06=cross color_06=green \
      leglabel_06='SDSS DR16 spectra' \
   layer_07=SkyEllipse \
      in_07=plots/plates-Mar2020.fits \
      lon_07=RACEN lat_07=DECCEN \
      shading_07=flat color_07=orange \
   layer_08=Mark \
      in_08=plots/spZbest-merged_with_targeting_info.fits icmd_08='select IS_TILED' \
      lon_08=PLUG_RA lat_08=PLUG_DEC \
      shading_08=auto color_08=ff6633 \
      leglabel_08='SDSS-IV/eFEDS spectra' \
   layer_09=SkyEllipse \
      in_09=plots/conflist_v6_0_4.fits \
      lon_09=RACEN lat_09=DECCEN \
      shading_09=translucent ellipse_09=filled_ellipse color_09=blue translevel_09=1.00 \
      leglabel_09='SDSS-V plates' \
   layer_10=Mark \
      in_10=plots/spAll-v6_0_4-eFEDS.fits \
      lon_10=PLUG_RA lat_10=PLUG_DEC \
      shading_10=auto color_10=blue \
      leglabel_10='SDSS-V spectra' \
   legseq=_06,_08,_09,_10 \
   out=plots/eFEDS_SDSS_map_plates_DR16_Mar2020_SDSSV_spectra.png



### these ones care about speccomp version:

stilts plot2sky \
   xpix=1800 ypix=900 \
   crowd=1.7 gridcolor=black gridaa=true texttype=antialias fontsize=20 \
   clon=135.98 clat=1.46 radius=5.538 \
   legend=true legpos=0.9958,0.019 \
   ra=1.49 \
   layer_01=Mark \
      in_01=plots/super-trim-efeds.txt ifmt_01=ASCII \
      lon_01=col1 lat_01=col2 \
      shading_01=auto size_01=3 \
   layer_02=Mark \
      in_02=plots/xray_outline.txt ifmt_02=ASCII \
      lon_02=col1 lat_02=col2 \
      shading_02=auto shape_02=filled_diamond color_02=blue \
   layer_03=SkyEllipse \
      in_03=plots/plates-dr16_eFEDS.fits icmd_03='select subset_legacy' \
      lon_03=RACEN lat_03=DECCEN \
      shading_03=flat color_03=green \
   layer_04=SkyEllipse \
      in_04=plots/plates-dr16_eFEDS.fits icmd_04='select subset_boss' \
      lon_04=RACEN lat_04=DECCEN \
      shading_04=flat color_04=blue \
   layer_05=SkyEllipse \
      in_05=plots/plates-dr16_eFEDS.fits icmd_05='select subset_segue' \
      lon_05=RACEN lat_05=DECCEN \
      shading_05=flat \
   layer_06=Mark \
      in_06=plots/specObj-dr16_eFEDS.fits \
      lon_06=PLUG_RA lat_06=PLUG_DEC \
      shading_06=auto shape_06=cross color_06=green \
      leglabel_06='SDSS DR16 spectra' \
   layer_07=SkyEllipse \
      in_07=plots/plates-Mar2020.fits \
      lon_07=RACEN lat_07=DECCEN \
      shading_07=flat color_07=orange \
   layer_08=Mark \
      in_08=plots/spZbest-merged_with_targeting_info.fits icmd_08='select IS_TILED' \
      lon_08=PLUG_RA lat_08=PLUG_DEC \
      shading_08=auto color_08=ff6633 \
      leglabel_08='SDSS-IV/eFEDS spectra' \
   layer_09=SkyEllipse \
      in_09=plots/conflist_v6_0_4.fits \
      lon_09=RACEN lat_09=DECCEN \
      shading_09=flat color_09=blue \
   layer_10=Mark \
      in_10=plots/spAll-v6_0_4-eFEDS.fits \
      lon_10=PLUG_RA lat_10=PLUG_DEC \
      shading_10=auto color_10=blue \
      leglabel_10='SDSS-V spectra' \
   layer_11=Mark \
      in_11=${VERSION}/spectra_compilation_eFEDS_${VERSION}.fits \
      lon_11=SPECZ_RAJ2000 lat_11=SPECZ_DEJ2000 \
      shading_11=auto shape_11=filled_diamond color_11=black icmd_11='select "SPECZ_RANK==1&&contains(ORIG_CATCODE,\"sdss\")==false&&contains(ORIG_CATCODE,\"boss\")==false&&contains(ORIG_CATCODE,\"efeds\")==false"' \
      leglabel_11='non-SDSS spectra' \
   legseq=_11,_06,_08,_10  \
   out=plots/eFEDS_SDSS_map_plates_DR16_Mar2020_SDSSV_nonSDSS_spectra.png

##
# qfstat "${VERSION}/spectra_compilation_eFEDS_${VERSION}.fits" SPECZ_CATCODE | sort -n -r -k 2 | gawk '$1!~/sdss|boss|efeds/{printf("%s,", $1)} END{print _}'
# LAMOST, gama, gaia_rvs, wigglez, ned, simbad, 2slaq, 6dFGS, 2mrs, hectospec, fast


# stilts plot2plane \
#    xpix=1024 ypix=800 \
#    xlabel=SN_MEDIAN_ALL ylabel='Number per bin' xcrowd=0.418 ycrowd=0.546 texttype=antialias fontsize=20 \
#    xmin=-0.2 xmax=20.1 ymin=0.0 ymax=210 \
#    legend=true legpos=0.9,0.9 \
#    x=SN_MEDIAN_ALL binsize=-206 barform=steps thick=3 \
#    layer_1=Histogram \
#       in_1=plots/spZbest-merged_with_targeting_info.fits \
#       color_1=ff6600 \
#       leglabel_1='SDSS-IV/eFEDS (March 2020) spectra' \
#    layer_2=Histogram \
#       in_2=plots/spAll-v6_0_2-eFEDS.fits icmd_2='select "OBJTYPE==\"QSO\""' \
#       color_2=blue \
#       leglabel_2='SDSS-V/eFEDS spectra' \
#    out=plots/eFEDS_SDSS_spectra_SN_histo.png






stilts plot2sky \
   xpix=1800 ypix=900 \
   crowd=1.7 gridcolor=black gridaa=true texttype=antialias fontsize=20 \
   clon=135.98 clat=1.46 radius=5.538 \
   legend=true legpos=0.9958,0.019 \
   layer_01=Mark \
      in_01=plots/super-trim-efeds.txt ifmt_01=ASCII \
      lon_01=col1 lat_01=col2 \
      shading_01=auto size_01=3 \
   layer_02=Mark \
      in_02=plots/xray_outline.txt ifmt_02=ASCII \
      lon_02=col1 lat_02=col2 \
      shading_02=auto shape_02=filled_diamond color_02=blue \
   layer_03=Mark \
      in_03=${VERSION}/eFEDS_C001_Main_PointSources_CTP_redshift_V17_efeds_speccomp_${VERSION}.fits \
      lon_03=CTP_LS8_RA lat_03=CTP_LS8_DEC \
      shading_03=auto shape_03=filled_diamond color_03=gray size_03=2 \
      icmd_03=''\
      leglabel_03='eFEDS Main Catalogue counterparts' \
   layer_04=Mark \
      in_04=${VERSION}/eFEDS_C001_Main_PointSources_CTP_redshift_V17_efeds_speccomp_${VERSION}.fits \
      lon_04=CTP_LS8_RA lat_04=CTP_LS8_DEC \
      shading_04=auto shape_04=filled_diamond color_04=blue size_04=2 \
      icmd_04='select "has_specz"' \
      leglabel_04='With good-quality spec-z' \
   legseq=_03,_04  \
    out=plots/eFEDS_SDSS_map_Xray_with_spec.png




stilts plot2plane \
   xpix=1120 ypix=832 \
   xlabel='LS8_r / ABmag' ylabel= texttype=antialias fontsize=24 \
   xmin=10 xmax=26 ymin=0 ymax=1400 \
   legend=true legpos=0.0231,0.968 \
   in=${VERSION}/eFEDS_C001_Main_PointSources_CTP_redshift_V17_efeds_speccomp_${VERSION}.fits x=LS8_r \
      binsize=0.2 barform=steps thick=3 \
   layer_1=Histogram \
      color_1=light_grey \
      leglabel_1='eFEDS main catalogue counterparts'  thick_1=5 \
   layer_2=Histogram \
      icmd_2='select "has_specz && SPECZ_NORMQ_specz>=3 && ( contains(SPECZ_CATCODE_specz,\"boss\") == true || contains(SPECZ_CATCODE_specz,\"sdss_vi\") == true)";' \
      color_2=cyan \
      leglabel_2='SDSS DR16 spec-z (good quality)' \
   layer_3=Histogram \
      icmd_3='select "has_specz && SPECZ_NORMQ_specz>=3 && contains(SPECZ_CATCODE_specz,\"efeds_vi\")";' \
      leglabel_3='SDSS-IV spec-z (good-quality)' \
   layer_4=Histogram \
      icmd_4='select "has_specz && SPECZ_NORMQ_specz>=3 && contains(SPECZ_CATCODE_specz,\"sdssv_vi\") == true";' \
      color_4=blue \
      leglabel_4='SDSS-V spec-z (good quality)' \
   layer_5=Histogram \
      icmd_5='select "has_specz && SPECZ_NORMQ_specz>=3 && contains(SPECZ_CATCODE_specz,\"efeds\") == false && contains(SPECZ_CATCODE_specz,\"sdss\") == false && contains(SPECZ_CATCODE_specz,\"boss\") == false";' \
      color_5=orange \
      leglabel_5='non-SDSS spec-z (good quality)' \
   layer_6=Histogram \
      icmd_6='select "has_specz && SPECZ_NORMQ_specz>=3";' \
      color_6=black \
      leglabel_6='With spec-z (good quality)'  thick_6=5 \
   layer_7=Histogram \
      icmd_7='select "has_specz";' \
      color_7=black \
      leglabel_7='With spec-z (any quality)'  thick_7=3 \
   legseq=_1,_7,_6,_2,_3,_4,_5  \
    out=plots/efeds_AGN_sdss_iv_v_public_histo_rmag.png




stilts plot2plane \
   xpix=1120 ypix=832 \
   xlabel='redshift (SPECZ_REDSHIFT_specz)' ylabel="Number per bin" texttype=antialias fontsize=24 \
   xmin=-0.1 xmax=4.1 ymin=0 ymax=1400 xcrowd=0.5 \
   legend=true legpos=0.6,0.968 \
   in=${VERSION}/eFEDS_C001_Main_PointSources_CTP_redshift_V17_efeds_speccomp_${VERSION}.fits \
   x='(min(4.05,SPECZ_REDSHIFT_specz))' \
      binsize=0.1 barform=steps thick=3 \
   layer_2=Histogram \
      icmd_2='select "has_specz && SPECZ_NORMQ_specz>=3 && ( contains(SPECZ_CATCODE_specz,\"boss\") == true || contains(SPECZ_CATCODE_specz,\"sdss_vi\") == true)";' \
      color_2=cyan \
      leglabel_2='SDSS DR16 spec-z (good quality)' \
   layer_3=Histogram \
      icmd_3='select "has_specz && SPECZ_NORMQ_specz>=3 && contains(SPECZ_CATCODE_specz,\"efeds_vi\")";' \
      leglabel_3='SDSS-IV/eFEDS spec-z (good-quality)' \
   layer_4=Histogram \
      icmd_4='select "has_specz && SPECZ_NORMQ_specz>=3 && contains(SPECZ_CATCODE_specz,\"sdssv_vi\") == true";' \
      color_4=blue \
      leglabel_4='SDSS-V spec-z (good quality)' \
   layer_5=Histogram \
      icmd_5='select "has_specz && SPECZ_NORMQ_specz>=3 && contains(SPECZ_CATCODE_specz,\"efeds\") == false && contains(SPECZ_CATCODE_specz,\"sdss\") == false && contains(SPECZ_CATCODE_specz,\"boss\") == false";' \
      color_5=orange \
      leglabel_5='non-SDSS spec-z (good quality)' \
   layer_6=Histogram \
      icmd_6='select "has_specz && SPECZ_NORMQ_specz>=3";' \
      color_6=black \
      leglabel_6='With spec-z (good quality)'  thick_6=5 \
   layer_7=Histogram \
      color_7=black \
      icmd_7='select "has_specz";' \
      leglabel_7='With spec-z (any quality)'  thick_7=3 \
   legseq=_7,_6,_2,_3,_4,_5  \
    out=plots/efeds_AGN_sdss_iv_v_public_histo_redshift.png




#
#
#
# stilts plot2sky \
#    xpix=1195 ypix=648 \
#    texttype=antialias fontsize=14 \
#    clon=135.82 clat=0.75 radius=5.373 \
#    legend=true legpos=0.9795,0.029 \
#    in=${VERSION}/eFEDS_C001_Main_PointSources_CTP_redshift_V17_efeds_speccomp_${VERSION}.fits lon=BEST_LS8_RA \
#     lat=BEST_LS8_Dec shading=auto \
#    layer_1=Mark \
#       color_1=grey \
#       leglabel_1='eFEDS main catalogue counterparts' \
#    layer_2=Mark \
#       icmd_2='select "ORIG_ID_COL_spec != \"-99\" && NORMQ_spec>=3 && startsWith(ORIG_ID_spec,\"125\") == false"' \
#       color_2=cyan \
#       leglabel_2='Public spec-z (good quality)' \
#    layer_3=Mark \
#       icmd_3='select "ORIG_ID_COL_spec != \"-99\" && NORMQ_spec>=3 && startsWith(ORIG_ID_spec,\"125\")"' \
#       leglabel_3='SDSS-IV spec-z (good-quality)' \
#    layer_4=Mark \
#       icmd_4='select has_spec_spall' \
#       color_4=blue \
#       leglabel_4='SDSS-V spec-z (any quality)' \
#     out=plots/efeds_AGN_sdss_iv_v_public_map.png
# #
#
