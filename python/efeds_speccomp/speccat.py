# encoding: utf-8
#
# @Author: Tom Dwelly
# @Date: Nov-2021
# @Filename: speccat.py
# @License: BSD 3-Clause
# @Copyright: Tom Dwelly

from __future__ import division
from __future__ import print_function
from __future__ import absolute_import
from __future__ import unicode_literals

import os
import re
import numpy as np
from astropy.io import fits
from astropy.time import Time
from astropy.coordinates import SkyCoord
from astropy import units
from astropy.coordinates import match_coordinates_sky, search_around_sky
# import pymangle as mangle
from scipy.constants import speed_of_light
from efeds_speccomp.print_func import print_comment  # , print_warning, print_error
from math import log10
import numpy.lib.recfunctions as rfn

speed_of_light_kms = speed_of_light / 1000.  # km/s

__invalid_mag_val = 99.9999
__invalid_mag_thresh = 90.0

z_std_thresh = 0.01

_flagmask = {
    'NO_LS': 0x0001,
    'Z_SCATTER': 0x0002,
    'ZCONF_RANGE': 0x0004,
    'BLAZAR_CANDIDATE': 0x0008,
    'NORMC_SCATTER': 0x0010,
}


def flux2mag(flux, zp=None):
    assert zp is not None
    if np.ndim(flux) > 0:
        with np.errstate(divide="ignore", invalid="ignore"):
            return np.nan_to_num(
                zp - 2.5 * np.log10(flux),
                copy=False,
                nan=__invalid_mag_val,
                posinf=__invalid_mag_val,
                neginf=__invalid_mag_val,
            )
    else:
        if flux <= 0.0:
            return __invalid_mag_val
        else:
            return zp - 2.5 * log10(flux)


def mag2flux(mag, zp=None):
    assert zp is not None
    if np.ndim(mag) > 0:
        with np.errstate(divide="ignore", invalid="ignore"):
            flux = np.where(
                mag >= __invalid_mag_thresh, 0.0, 10.0 ** (-0.4 * (mag - zp))
            )
    else:
        if mag >= __invalid_mag_thresh:
            flux = 0.0
        else:
            flux = 10.0 ** (-0.4 * (mag - zp))
    return flux


def AB2nMgy(mag):
    return mag2flux(mag, 22.50)


def nMgy2AB(flux):
    return flux2mag(flux, 22.50)


# #####
def filter_on_expr(reca=None,
                   expr=None):
    if expr is None:
        return reca
    assert isinstance(reca, np.recarray)

    rows_before = len(reca)
    colnames = reca.columns.names
    # turn the expr into a valid filtering expression - TODO
    # look for tokens in the string that are columns in the table
    expr_split = [s.strip() for s in re.split('(<|>|=|\(|\)|\!)', expr)]
    norm_expr = "".join([f"reca[\"{s}\"]" if s in colnames else s for s in expr_split])
    # norm_expr = f"np.where({norm_expr}, True, False)"
    # print(norm_expr)
    # print(reca.dtype)
    # print(f'About to filter table (with expr: {norm_expr})', flush=True)

    good_mask = eval(norm_expr)
    # print(f'len(reca): {len(reca)})', flush=True)
    # print(f'len(good_mask): {len(good_mask)})', flush=True)
    # print(f'np.count_nonzero(good_mask): {np.count_nonzero(good_mask)})', flush=True)

    reca = np.extract(good_mask, reca)
    print_comment(f'Filtering table (with expr: {norm_expr}) '
                  f'num rows before= {rows_before} => after= {len(reca)}')
    return reca

# https://ned.ipac.caltech.edu/help/ui/nearposn-list_objecttypes
# https://simbad.u-strasbg.fr/simbad/sim-display?data=otypes
#


_known_classes = [
    ('QSO', np.char.upper(['QSO', 'Q', 'AGN', 'Sy1', 'Sy2', 'BLAGN', 'LIN', 'RG',
                           'Q_Lens', 'SYG', '-9', 'SPARE_RADIO', ])),
    ('GALAXY', np.char.upper(['GALAXY', 'GAL', 'G', 'GiC', 'BCG', 'EmG', 'CGC',
                              'CLG', 'LEG', 'GCLSTR', 'WIG_SDSS_EXT', 'WIG_SDSS',
                              'H2G', 'BIC', 'CGG', 'GIG', 'GGROUP', 'PAG', 'GIP',
                              'GRG', 'GPAIR', 'C?G', 'POFG', 'GLE', 'HII', 'HI',
                              '0', '-1', '-2', '-3', '-4', '-5', '-6', '-7',
                              '1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                              '11', '12', '15', '16', '19', '20', '98',
                              '1A', '2A', '3A', '4A', '5A',
                              '1B', '2B', '3B', '4B', '20B', '5A5S', '-1X5T',
                              '5:4', '1:1', '3:3', '7:1', '20X', '0__P', '1X',
                              '-1B_R', '2B3T', 'LSB', 'LSB_G', ])),
    ('STAR', np.char.upper(['STAR', '*', '**', 'V*', 'AB*', 'BS*', 'WD*', 'LM*',
                            'PE*', 'HB*', 'C*', 'PM*', 'RR*', 'EB?',
                            'RG*', '!*', 'LP*', 'SN*', 'BLUE*', '!V*',
                            'DS*', 'HS*', 'BD*', 'EB*', 'SB*', 'RO*', 'CV*', 'HV*',
                            ])),

    ('QSO_BAL', np.char.upper(['QSO_BAL', 'BAL', 'BALQSO', ])),
    ('BLAZAR', np.char.upper(['BLAZAR', 'BLAZARCANDIDATE', 'BLL', 'BLLac', 'BLA', 'BL?', ])),
    ('UNKNOWN', np.char.upper(['UNKNOWN', '?', '', 'RADIOS',
                               'SPARE_GALEX', 'IRS', 'UVS', 'UVES', 'VISS', 'GAMMAS',
                               'VID', 'G?', 'HB?', 'BD?', 'Q?', 'gB', 'SN?', 'AG?',
                               'LI?', 'HS?', 'RB?', 'NIR', 'Le?', ])),
]


# ##
def norm_class(oc=None):
    # map incoming orig_class values into one of GALAXY, QSO, STAR, NONE

    ocu = np.char.strip(np.char.upper(oc))
    isin_any = np.zeros(len(oc), dtype=bool)
    # result = np.full(len(oc), 'UNKNOWN', dtype='<U8')
    for (n, v) in _known_classes:
        isin = np.isin(ocu, v)
        ocu[isin] = n
        isin_any[isin] = True

    ocu[~isin_any] = 'UNCLASS'

    return ocu.astype('<U8')


# read in the photometric reference catalogue
def load_ls(ls_filename=None,
            pm_snr_min=2.0,
            mask=None):

    assert ls_filename is not None
    filename = os.path.expanduser(ls_filename)
    if os.path.isfile(filename) is False:
        raise Exception(f"Error finding photometric catalogue: {filename}")
    try:
        hdul = fits.open(filename)
    except:
        raise Exception(f"Could not open photometric catalogue: {filename}")

    ls = hdul[1].data
    hdul.close()
    print_comment("",
                  f"Reading photometric catalogue: {filename}",
                  f"Photometric catalogue contains {len(ls)} rows")

    # guess the DR from the RELEASE
    dr_sample = ls['RELEASE'][0]
    ls_dr = int(dr_sample / 1000)
    assert ls_dr >= 7
    assert ls_dr <= 20
    if ls_dr >= 10:
        ref_epoch = 2016.0
    else:
        ref_epoch = 2015.5

    print_comment(f"This looks like lsdr{ls_dr} with default_epoch={ref_epoch}")

    # remove sources with type=='DUP'
    # no no no! dup_mask = np.where(ls['GAIA_DUPLICATED_SOURCE'], True, False)
    dup_mask = np.where(ls['TYPE'] == 'DUP', True, False)
    ls = np.extract(~dup_mask, ls)
    print_comment(f"After filtering objects with type='DUP', ls catalogue "
                  f"contains: {len(ls)} rows")

    if mask is not None:
        # filter on super trim mangle
        good_mask = mask.contains(ls['RA'], ls['DEC'])
        ls = np.extract(good_mask, ls)
        print_comment(f"After filtering on trim mask, ls catalogue "
                      f"contains: {len(ls)} rows")

    # Normalise the pms
    ls['PMRA'] = np.nan_to_num(ls['PMRA'], nan=0.0, posinf=0.0, neginf=0.0)
    ls['PMDEC'] = np.nan_to_num(ls['PMDEC'], nan=0.0, posinf=0.0, neginf=0.0)
    ls['PMRA_IVAR'] = np.nan_to_num(ls['PMRA_IVAR'], nan=0.0, posinf=0.0, neginf=0.0)
    ls['PMDEC_IVAR'] = np.nan_to_num(ls['PMDEC_IVAR'], nan=0.0, posinf=0.0, neginf=0.0)

    pm_mask = np.where((np.square(ls['PMRA']) * ls['PMRA_IVAR'] +
                        np.square(ls['PMDEC']) * ls['PMDEC_IVAR']) < pm_snr_min**2)

    ls['PMRA'][pm_mask] = 0.0
    ls['PMDEC'][pm_mask] = 0.0

    ls['REF_EPOCH'] = np.where(ls['REF_EPOCH'] > 0., ls['REF_EPOCH'], ref_epoch)
    # ls_time = Time(val=2015.5, format='decimalyear')
    ls_time = Time(val=ls['REF_EPOCH'], format='decimalyear')

    print_comment(f'Significant (>{pm_snr_min} sigma) proper motion '
                  f'for {np.count_nonzero(ls["PMRA"])} stars')

    coords_ls = SkyCoord(
        frame='icrs',
        obstime=ls_time,
        ra=ls['RA'] * units.degree,
        dec=ls['DEC'] * units.degree,
        pm_ra_cosdec=ls['PMRA'] * units.mas / units.yr,
        pm_dec=ls['PMDEC'] * units.mas / units.yr,
        # distance=Distance(parallax=np.maximum(0.1,ls['PARALLAX']) * units.mas),
    )

    if 'LS_ID' not in ls.dtype.names:
        print_comment("Computing new LS_ID column")
        # then add it in
        ls_id = (ls['OBJID'].astype(np.ulonglong) +
                 (ls['BRICKID'].astype(np.ulonglong) << 16) +
                 (ls['RELEASE'].astype(np.ulonglong) << 40))
        # print(ls_id.dtype, flush=True)
        ls = rfn.append_fields(ls,
                               'LS_ID',
                               ls_id,
                               dtypes=np.ulonglong,
                               usemask=False,
                               asrecarray=True)
        # print(ls.dtype, flush=True)

    return ls, coords_ls

def calc_basic_stats(specz, ti):
    result = {
        'NUM_NON_NANS': 0,
        'NORMQ_MIN': 0,
        'NORMQ_MAX': 0,
        'NORMQ_MEAN': np.nan,
        'Z_MEAN': np.nan,
        'Z_MEDIAN': np.nan,
        'Z_STDDEV': np.nan,
    }
    if len(ti) > 0:
        result['NUM_NON_NANS'] = np.count_nonzero(~np.isnan(specz['ORIG_REDSHIFT'][ti]))
        if result['NUM_NON_NANS'] > 0:
            result['NORMQ_MIN'] = np.nanmin(specz['ORIG_NORMQ'][ti])
            result['NORMQ_MAX'] = np.nanmax(specz['ORIG_NORMQ'][ti])
            result['NORMQ_MEAN'] = np.nanmean(specz['ORIG_NORMQ'][ti])
            result['Z_MEAN'] = np.nanmean(specz['ORIG_REDSHIFT'][ti])
            result['Z_MEDIAN'] = np.nanquantile(specz['ORIG_REDSHIFT'][ti], 0.5)
            result['Z_STDDEV'] = np.nanstd(specz['ORIG_REDSHIFT'][ti])

    return result


def choose_from_one(specz, ti, ti_chosen, stats=["SEL", ]):
    # assume that ti_chosen is a single index
    specz['SPECZ_NSEL'][ti] = 1
    specz['SPECZ_CATCODE'][ti] = specz['ORIG_CATCODE'][ti_chosen]
    specz['SPECZ_RANK'][ti_chosen] = 1
    specz['SPECZ_REDSHIFT'][ti] = specz['ORIG_REDSHIFT'][ti_chosen]
    specz['SPECZ_NORMQ'][ti] = specz['ORIG_NORMQ'][ti_chosen]
    specz['SPECZ_NORMC'][ti] = specz['ORIG_NORMC'][ti_chosen]
    if stats is not None:
        for s in stats:
            specz[f'SPECZ_{s}_NORMQ_MAX'][ti] = specz['ORIG_NORMQ'][ti_chosen]
            specz[f'SPECZ_{s}_NORMQ_MEAN'][ti] = specz['ORIG_NORMQ'][ti_chosen]
            specz[f'SPECZ_{s}_Z_MEAN'][ti] = specz['ORIG_REDSHIFT'][ti_chosen]
            specz[f'SPECZ_{s}_Z_MEDIAN'][ti] = specz['ORIG_REDSHIFT'][ti_chosen]
            specz[f'SPECZ_{s}_Z_STDDEV'][ti] = 0.0

    # return specz


def select_best_catcode(catcodes, catcode_rank):
    i = np.argmin([catcode_rank[catcode] for catcode in catcodes])
    try:
        i = i[0]
    except:
        pass
    return i, catcodes[i]


def resolve(specz, idx, ti, catcode_rank):
    '''
    Compute final redshift++ and calculate test statistics across several
    specz for a single ls_id
    Inputs:
        specz   - the np.recarray holding all of the specz
        idx     - the index of the entry in the ls recarray
        ti      - np.array of indices into the specz array for this ls_id/ls_idx
        catcode_rank - convert from a catcode to a numeric rank
    '''
    if idx >= 0:
        flags = 0
        specz['SPECZ_N'][ti] = len(ti)
        specz['SPECZ_BITMASK'][ti] = np.bitwise_or.reduce(specz['ORIG_BITMASK'][ti])
        specz['SPECZ_RANK'][ti] = 3  # some of these will be overwritten later

        # compute and copy some stats over all options
        stats_all = calc_basic_stats(specz, ti)
        for s in ['NORMQ_MEAN', 'NORMQ_MAX', 'Z_MEAN', 'Z_MEDIAN', 'Z_STDDEV']:
            specz[f'SPECZ_ALL_{s}'][ti] = stats_all[s]

        # flag up any blazar classifications
        if ((stats_all['NORMQ_MIN'] == -1) | ('BLAZAR' in specz['ORIG_NORMC'][ti])):
            flags = flags & _flagmask['BLAZAR_CANDIDATE']

        # Find out which ones have VI and count them
        ti_vi = ti[specz['ORIG_HASVI'][ti]]
        specz['SPECZ_ALL_NVI'][ti] = len(ti_vi)

        # if we have >1 VI then consider only those ones
        if len(ti_vi) > 0:
            ti_sel = ti_vi
            specz['SPECZ_HASVI'][ti] = True
        else:
            ti_sel = ti

        # now choose only the subset that have ORIG_NORMQ = max(ORIG_NORMQ)
        normq_max = np.nanmax(specz['ORIG_NORMQ'][ti_sel])
        ti_sel = ti_sel[specz['ORIG_NORMQ'][ti_sel] == normq_max]
        specz['SPECZ_NORMQ'][ti] = normq_max

        assert len(ti_sel) > 0

        specz['SPECZ_SEL_BITMASK'][ti] = np.bitwise_or.reduce(specz['ORIG_BITMASK'][ti_sel])
        if len(ti_sel) == 1:
            # only one clear option, so logic is trivial
            choose_from_one(specz, ti, ti_sel[0], stats=["SEL", ])
        else:
            specz['SPECZ_NSEL'][ti] = len(ti_sel)
            specz['SPECZ_RANK'][ti_sel] = 2  # ditto

            stats_sel = calc_basic_stats(specz, ti_sel)
            for s in ['NORMQ_MEAN', 'NORMQ_MAX', 'Z_MEAN', 'Z_MEDIAN', 'Z_STDDEV']:
                specz[f'SPECZ_SEL_{s}'][ti] = stats_sel[s]

            # check if there is significant scatter in the redshifts
            if stats_sel['Z_STDDEV'] / (max(0.0, stats_sel['Z_MEAN']) + 1.) > z_std_thresh:
                flags = flags & _flagmask['Z_SCATTER']

            # check if there is significant scatter in the normc
            u_normc, n_normc = np.unique(specz['ORIG_NORMC'][ti_sel], return_counts=True)
            if len(u_normc) > 1:
                flags = flags & _flagmask['NORMC_SCATTER']

            # choose a single best specz
            # base this on the name of the catalogue it comes from
            i_best, catcode_best = select_best_catcode(specz['ORIG_CATCODE'][ti_sel], catcode_rank)
            ti_best = [ti_sel[i_best]]
            specz['SPECZ_RANK'][ti_best] = 1
            specz['SPECZ_REDSHIFT'][ti] = specz['ORIG_REDSHIFT'][ti_best]
            specz['SPECZ_NORMQ'][ti] = specz['ORIG_NORMQ'][ti_best]
            specz['SPECZ_NORMC'][ti] = specz['ORIG_NORMC'][ti_best]
            specz['SPECZ_CATCODE'][ti] = specz['ORIG_CATCODE'][ti_best]
            # # or take the majority decision
            # specz['SPECZ_NORMC'][ti] = u_normc[np.argmax(n_normc)]

    else:
        flags = _flagmask['NO_LS']
        specz['SPECZ_RAJ2000'][ti] = specz['ORIG_RA'][ti]  # ignore PMs
        specz['SPECZ_DEJ2000'][ti] = specz['ORIG_DEC'][ti]
        for tii in ti:
            choose_from_one(specz, np.array([tii, ]), tii, stats=["SEL", "ALL", ])

    specz['SPECZ_FLAGS'][ti] = flags



def write_to_fits(specz, inspec, filename):
    #write out the results
    hdu_out = fits.BinTableHDU.from_columns(specz)

    # write some keywords
    for i, c in enumerate(inspec):
        hdu_out.header[f'CTNAME{i:02d}'] = c.name
        hdu_out.header[f'CTVER{i:02d}'] = c.release
        hdu_out.header[f'CTFILE{i:02d}'] = c.filename
        hdu_out.header[f'CTMASK{i:02d}'] = c.bitmask
        hdu_out.header[f'CTBIT{i:02d}'] = bin(c.bitmask)
        hdu_out.header[f'CTHEX{i:02d}'] = hex(c.bitmask)
        hdu_out.header[f'CTRA{i:02d}'] = c.col_ra
        hdu_out.header[f'CTDEC{i:02d}'] = c.col_dec
        hdu_out.header[f'CTID{i:02d}'] = c.col_id
        hdu_out.header[f'CTQUAL{i:02d}'] = c.col_qual
        hdu_out.header[f'CTZ{i:02d}'] = c.col_z
        hdu_out.header[f'CTCZ{i:02d}'] = c.col_z_is_cz

    filename = os.path.expanduser(filename)
    hdu_out.writeto(filename, overwrite=True, checksum=True)

    # now describe the content for the wiki
    s = '''
    | Column name          |  Column Format | Units | Description |
    | LS_IDX               |   K |     | Index of the legacysurvey object in the input file - do not use for anything serious |
    | LS_ID                |   K |     | Unique identifier for the legacysurvey object. Derived from orig LS columns as follows: LS_ID = OBJID + (BRICKID << 16) + (RELEASE << 40) |
    | LS_RA                |   D | deg | Coordinate of this legacysurvey object |
    | LS_DEC               |   D | deg | Coordinate of this legacysurvey object |
    | LS_PMRA              |   E | mas/yr | Proper motion of this legacysurvey object |
    | LS_PMDEC             |   E | mas/yr | Proper motion of this legacysurvey object |
    | LS_EPOCH             |   E | yr  | Epoch of the legacysurvey coordinates |
    | LS_MAG_G             |   E | mag | g-band AB magnitude of the legacysurvey object (from FLUX_G) |
    | LS_MAG_R             |   E | mag | r-band AB magnitude of the legacysurvey object (from FLUX_R) |
    | LS_MAG_Z             |   E | mag | z-band AB magnitude of the legacysurvey object (from FLUX_Z) |
    | SPECZ_N              |   J |     | Number of spec-z available for this object |
    | SPECZ_RAJ2000        |   D | deg | Coordinate for this specz object, at epoch 2000 |
    | SPECZ_DEJ2000        |   D | deg | Coordinate for this specz object, at epoch 2000 |
    | SPECZ_NSEL           |   J |     | Number of spec-z selected to inform result for this object |
    | SPECZ_RANK           |   J |     | Rank of this particular spec-z (1=best, >1=not best, 0=not selected) |
    | SPECZ_REDSHIFT       |   E |     | Final redshift determined for this object |
    | SPECZ_NORMQ          |   J |     | Final normalised redshift quality associated with this object |
    | SPECZ_NORMC          |  8A |     | Final normlised classfication determined for this object |
    | SPECZ_HASVI          |   L |     | True if best spec-z for this object has a visual inspection |
    | SPECZ_CATCODE        | 12A |     | Catalogue code of 'best' spec-z for this object |
    | SPECZ_BITMASK        |   K |     | Bitmask encoding which input catalogues contained specz for this object |
    | SPECZ_SEL_BITMASK    |   K |     | Bitmask encoding which input catalogues contained informative specz for this object |
    | SPECZ_FLAGS          |   J |     | Bitmask encoding quality flags for this object |
    | SPECZ_SEL_NORMQ_MAX  |   E |     | Highest NORMQ of informative spec-z for this object |
    | SPECZ_SEL_NORMQ_MEAN |   E |     | Mean NORMQ of informative spec-z for this object |
    | SPECZ_SEL_Z_MEAN     |   E |     | Mean REDSHIFT of informative spec-z for this object |
    | SPECZ_SEL_Z_MEDIAN   |   E |     | Median REDSHIFT of informative spec-z for this object |
    | SPECZ_SEL_Z_STDDEV   |   E |     | Standard deviation of REDSHIFTs for informative spec-z for this object |
    | SPECZ_ALL_NVI        |   J |     | Number of spec-z with VIs for this object  |
    | SPECZ_ALL_NORMQ_MAX  |   E |     | Highest NORMQ of all spec-z for this object   |
    | SPECZ_ALL_NORMQ_MEAN |   E |     | Mean NORMQ of all spec-z for this object  |
    | SPECZ_ALL_Z_MEAN     |   E |     | Mean REDSHIFT of all spec-z for this object  |
    | SPECZ_ALL_Z_MEDIAN   |   E |     | Median REDSHIFT of all spec-z for this object  |
    | SPECZ_ALL_Z_STDDEV   |   E |     | Standard deviation of REDSHIFTs for all spec-z for this object  |
    | ORIG_CATCODE         | 12A |     | Catalogue code of individual spec-z  |
    | ORIG_RA              |   D | deg | Coordinate of individual spec-z  |
    | ORIG_DEC             |   D | deg | Coordinate of individual spec-z  |
    | ORIG_POS_EPOCH       |   E | yr  | Coordinate epoch of individual spec-z  |
    | ORIG_LS_SEP          |   E | arcsec | Distance from spec-z to nearest photometric ctp (coords corrected for pm)  |
    | ORIG_LS_GT1CTP       |   L |     | True if this spec-z can be associated with more than one photometric counterpart |
    | ORIG_LS_RANK_CTP     |   J |     | Rank of this photometric counterpart out of all possibilities |
    | ORIG_ID_COL          | 16A |     | Name of column in original catalogue supplying ORIG_ID value |
    | ORIG_QUAL_COL        | 16A |     | Name of column in original catalogue supplying ORIG_QUAL info  |
    | ORIG_REDSHIFT_COL    | 16A |     | Name of column in original catalogue supplying ORIG_REDSHIFT info  |
    | ORIG_CLASS_COL       | 16A |     | Name of column in original catalogue supplying ORIG_CLASS info  |
    | ORIG_ID              | 32A |     | Original value of ID (represented as a string)  |
    | ORIG_REDSHIFT        |   E |     | Original redshift value  |
    | ORIG_QUAL            |  8A |     | Original quality value  |
    | ORIG_NORMQ           |   J |     | Normalised quality metric |
    | ORIG_CLASS           | 16A |     | Original classification label  |
    | ORIG_HASVI           |   L |     | True if specz has a visual inspection  |
    | ORIG_NORMC           |  8A |     | Normalised classification code  |
    | ORIG_BITMASK         |   K |     | Constribution of this specz to the SPECZ_*BITMASK columns |
    '''
    print('## Column description table\n',
          s,
          '##')


# ##############
class Epoch(object):
    def __init__(self,
                 val=2000.0,
                 type='const',
                 format='decimalyear',
                 time=None,):

        self.type = type.lower()

        if self.type == 'const':
            if time is None:
                self.format = format
                self.time = Time(val=val, format=self.format)
            else:
                self.time = time
                self.format = None
        elif self.type == 'col':
            assert format is not None
            self.time = None
            self.format = format
        else:
            raise Exception(f"Unknown epoch type: {type}")



# ##############
class SpecCat(object):
    def __init__(self,
                 name=None,
                 release='',
                 filename='',
                 col_ra='ra',
                 col_dec='dec',
                 col_z='redshift',
                 col_z_is_cz=False,
                 col_class='class',
                 col_id='ID',
                 col_qual='QUALITY',
                 criteria=[],
                 match_radius=1.0,
                 fiber_diam=2.0,
                 epoch=Epoch(2000.0),
                 ref='',
                 catalog_origin='',
                 team_url='',
                 normq=None,
                 notes='',
                 col_nvi=None,
                 rank=0,
                 ):

        assert name is not None
        assert normq is not None
        assert isinstance(epoch, Epoch)

        self.name = name
        self.release = release
        self.filename = os.path.expanduser(filename)
        self.col_ra = col_ra
        self.col_dec = col_dec
        self.col_z = col_z
        self.col_z_is_cz = col_z_is_cz
        self.col_class = col_class
        self.col_id = col_id
        self.col_qual = col_qual
        self.criteria = criteria
        self.match_radius = match_radius * units.arcsec
        self.fiber_diam = fiber_diam * units.arcsec
        self.epoch = epoch
        self.ref = ref
        self.catalog_origin = catalog_origin
        self.team_url = team_url
        self.normq = normq
        self.notes = notes
        self.col_nvi = col_nvi
        self.rank = rank

        self.coords = None
        self.spectra = None

    def set_bit(self, bit=None):
        assert bit >= 0
        self.bit = bit
        self.bitmask = 1 << bit

    def prep(self, mask=None):
        assert self.filename is not None
        assert mask is not None

        print_comment("", f"Working on catalogue {self.name}: {self.filename}")
        if os.path.isfile(self.filename) is False:
            raise Exception(f"Error finding spectroscopic catalogue: {self.filename}")

        try:
            hdul = fits.open(self.filename)
        except:
            raise Exception(f"Could not open spectroscopic catalogue: {self.filename}")

        s = hdul[1].data
        hdul.close()

        self.initial_len = len(s)
        print_comment(f"Initially, catalogue for {self.name} contains: {self.initial_len} rows")

        if self.col_z_is_cz:
            print_comment(f"Treating column {self.col_z} as cz (i.e. not plain redshift)")

        # filter on super trim mangle
        good_mask = mask.contains(s[self.col_ra],
                                  s[self.col_dec])
        s = np.extract(good_mask, s)
        self.trim_len = len(s)
        print_comment(f"After filtering on trim mask {self.name} "
                      f"contains: {self.trim_len} rows")

        # print (s.dtype)

        # filter on catalogue-specific criteria
        for expr in self.criteria:
            s = filter_on_expr(s, expr)

        self.n_filtered = len(s)

        self.spectra = s

        coords = SkyCoord(frame='icrs',
                          ra=s[self.col_ra] * units.degree,
                          dec=s[self.col_dec] * units.degree)

        self.coords = coords

        # now get the vital input info into the standard format
        specz = np.zeros(
            len(s),
            dtype=[
                ('LS_IDX', np.int64),
                ('LS_ID', np.int64),
                ('LS_RA', np.float64),
                ('LS_DEC', np.float64),
                ('LS_PMRA', np.float32),
                ('LS_PMDEC', np.float32),
                ('LS_EPOCH', np.float32),
                ('LS_MAG_G', np.float32),
                ('LS_MAG_R', np.float32),
                ('LS_MAG_Z', np.float32),
                ('SPECZ_N', np.int32),
                ('SPECZ_RAJ2000', np.float64),
                ('SPECZ_DEJ2000', np.float64),
                ('SPECZ_NSEL', np.int32),
                ('SPECZ_RANK', np.int32),
                ('SPECZ_REDSHIFT', np.float32),
                ('SPECZ_NORMQ', np.int32),
                ('SPECZ_NORMC', '<U8'),
                ('SPECZ_HASVI', bool),
                ('SPECZ_CATCODE', '<U12'),
                ('SPECZ_BITMASK', np.int64),
                ('SPECZ_SEL_BITMASK', np.int64),
                ('SPECZ_FLAGS', np.int32),
                ('SPECZ_SEL_NORMQ_MAX', np.int32),
                ('SPECZ_SEL_NORMQ_MEAN', np.float32),
                ('SPECZ_SEL_Z_MEAN', np.float32),
                ('SPECZ_SEL_Z_MEDIAN', np.float32),
                ('SPECZ_SEL_Z_STDDEV', np.float32),
                ('SPECZ_ALL_NVI', np.int32),
                ('SPECZ_ALL_NORMQ_MAX', np.int32),
                ('SPECZ_ALL_NORMQ_MEAN', np.float32),
                ('SPECZ_ALL_Z_MEAN', np.float32),
                ('SPECZ_ALL_Z_MEDIAN', np.float32),
                ('SPECZ_ALL_Z_STDDEV', np.float32),
                ('ORIG_CATCODE', '<U12'),
                ('ORIG_RA', np.float64),
                ('ORIG_DEC', np.float64),
                ('ORIG_POS_EPOCH', np.float32),
                ('ORIG_LS_SEP', np.float32),
                ('ORIG_LS_GT1CTP', bool),
                ('ORIG_LS_CTP_RANK', np.int32),
                ('ORIG_ID_COL', '<U16'),
                ('ORIG_QUAL_COL', '<U16'),
                ('ORIG_REDSHIFT_COL', '<U16'),
                ('ORIG_CLASS_COL', '<U16'),
                ('ORIG_ID', '<U32'),
                ('ORIG_REDSHIFT', np.float32),
                ('ORIG_QUAL', '<U8'),
                ('ORIG_NORMQ', np.int32),
                ('ORIG_CLASS', '<U16'),
                ('ORIG_HASVI', bool),
                ('ORIG_NORMC', '<U8'),
                ('ORIG_BITMASK', np.int64),
            ])

        specz['ORIG_RA'] = s[self.col_ra]
        specz['ORIG_DEC'] = s[self.col_dec]
        specz['ORIG_POS_EPOCH'] = self.epoch.time.decimalyear
        if self.col_z_is_cz:
            specz['ORIG_REDSHIFT'] = np.divide(s[self.col_z], speed_of_light_kms)
        else:
            specz['ORIG_REDSHIFT'] = s[self.col_z]

        specz['ORIG_CATCODE'] = self.name
        if self.col_id is not None:
            specz['ORIG_ID_COL'] = self.col_id
            specz['ORIG_ID'] = s[self.col_id]

        specz['ORIG_REDSHIFT_COL'] = self.col_z

        if self.col_qual is not None:
            specz['ORIG_QUAL_COL'] = self.col_qual
            specz['ORIG_QUAL'] = s[self.col_qual].astype('<U8')

        if self.col_class is not None:
            specz['ORIG_CLASS_COL'] = self.col_class
            if self.col_class.startswith('const='):
                cl = self.col_class[6:]
                specz['ORIG_CLASS'] = cl
            else:
                specz['ORIG_CLASS'] = s[self.col_class]

        specz['ORIG_NORMC'] = norm_class(specz['ORIG_CLASS'])

        specz['ORIG_BITMASK'] = self.bitmask

        if self.col_nvi is not None:
            specz['ORIG_HASVI'] = np.where(s[self.col_nvi] > 0, True, False)


        if callable(self.normq):
            specz['ORIG_NORMQ'] = self.normq(s)
        elif isinstance(self.normq, str):
            specz['ORIG_NORMQ'] = s[self.normq]
        else:
            specz['ORIG_NORMQ'] = self.normq


        self.specz = specz

    #
    def match_to_ls(self, cat_ls=None, coords_ls=None,):
        assert coords_ls is not None
        assert cat_ls is not None
        assert self.coords is not None

        # # keep all of the ls matches to allow proper flagging
        # idx1, idx2, d2d, d3d = search_around_sky(
        #     self.coords,
        #     ls['coords'].apply_space_motion(new_obstime=self.epoch.time),
        #     self.match_radius)
        #
        # self.n_with_ls = len(np.unique(idx1))
        # print_comment(f"{self.name:8} match to ls catalogue: "
        #               f"{self.n_with_ls} / {self.n_filtered} matches "
        #               f"within radius={self.match_radius}")

        coords_ls_new = coords_ls.apply_space_motion(new_obstime=self.epoch.time)
        # now just look for the nearest match
        idx, d2d, d3d = match_coordinates_sky(
            self.coords,
            coords_ls_new,
            nthneighbor=1,
            storekdtree=True)
        self.with_ls = np.where(d2d <= self.match_radius, True, False)
        self.n_with_ls = np.count_nonzero(self.with_ls)

        print_comment(f"{self.name:8} nearest match to ls catalogue: "
                      f"{self.n_with_ls} / {self.n_filtered} matches "
                      f"within radius={self.match_radius}")

        self.ls_idx = np.where(self.with_ls, idx, None)
        self.ls_ids = np.where(self.with_ls, cat_ls['LS_ID'][idx], 0)
        self.ls_sep = d2d

        # copy the LS info into the specz arrays
        self.specz['LS_IDX'] = np.where(self.with_ls, idx, -1)
        self.specz['LS_ID'] = np.where(self.with_ls, cat_ls['LS_ID'][idx], 0)
        self.specz['LS_RA'] = np.where(self.with_ls, cat_ls['RA'][idx], np.nan)
        self.specz['LS_DEC'] = np.where(self.with_ls, cat_ls['DEC'][idx], np.nan)
        self.specz['LS_PMRA'] = np.where(self.with_ls, cat_ls['PMRA'][idx], np.nan)
        self.specz['LS_PMDEC'] = np.where(self.with_ls, cat_ls['PMDEC'][idx], np.nan)
        self.specz['LS_EPOCH'] = np.where(self.with_ls, cat_ls['REF_EPOCH'][idx], np.nan)
        self.specz['LS_MAG_G'] = np.where(self.with_ls, nMgy2AB(cat_ls['FLUX_G'][idx]), np.nan)
        self.specz['LS_MAG_R'] = np.where(self.with_ls, nMgy2AB(cat_ls['FLUX_R'][idx]), np.nan)
        self.specz['LS_MAG_Z'] = np.where(self.with_ls, nMgy2AB(cat_ls['FLUX_Z'][idx]), np.nan)

        coords_2000 = coords_ls.apply_space_motion(new_obstime=Time(2000.0, format='decimalyear'))

        self.specz['SPECZ_RAJ2000'] = np.where(self.with_ls, coords_2000.ra.deg[idx], np.nan)
        self.specz['SPECZ_DEJ2000'] = np.where(self.with_ls, coords_2000.dec.deg[idx], np.nan)

        self.specz['ORIG_LS_SEP'] = self.ls_sep.arcsec
        self.specz['ORIG_LS_CTP_RANK'] = 1

        # print_comment(f"{self.name:8} test_idx: {np.count_nonzero(self.ls_idx)}")
        # print_comment(f"{self.name:8} test_ids: {np.count_nonzero(self.ls_ids)}")

        if len(d2d) > 0:

            self.sep_quantile_levels = [0.5, 0.9, 0.95, 0.99, 0.999, 1.0]
            self.sep_quantiles = np.quantile(d2d.arcsec, self.sep_quantile_levels)
            str_sep_quantile_levels = [f"{q:6.3f} " for q in self.sep_quantile_levels]
            str_sep_quantiles = [f"{q:6.3f} " for q in self.sep_quantiles]
            print_comment(f"{self.name:8} nearest match to ls quantiles: " +
                          " ".join(str_sep_quantile_levels))
            print_comment(f"{self.name:8} nearest match to ls quantiles: " +
                          " ".join(str_sep_quantiles))


        # Find max number of multiple neighbours within the matching radius
        idxs, idxl, d2d, d3d = search_around_sky(self.coords, coords_ls_new, self.match_radius)
        if len(idxs) > 0:
            idxsu, nrep = np.unique(idxs, return_counts=True)
            nmax = max(nrep)

            #self.with_gt1ctp = np.zeros(len(self.specz), dtype=bool)
            #self.nctp = np.zeros(len(self.specz), dtype=np.int32)
            ndups_tot = 0
            print_comment(f"{self.name:8} max number of ls ctps per specz is: {nmax}")
            if nmax > 1:
                for nn in np.arange(2, nmax + 1):
                    # check how many have an nth neighbour within the matching radius
                    idxb, d2db, d3db = match_coordinates_sky(
                        self.coords,
                        coords_ls_new,
                        nthneighbor=nn)
                    maskn = np.where(d2db <= self.match_radius, True, False)
                    ndups = np.count_nonzero(maskn)
                    ndups_tot = ndups_tot + ndups
                    print_comment(f"{self.name:8} Number of specz with {nn} ls ctps: {ndups}")
                    mask_dest = np.arange(len(self.specz), len(self.specz) + ndups)
                    # extend the specz array by as many rows as required
                    self.specz = np.lib.pad(self.specz, (0, ndups),)
                    # print_comment(f"{self.name:8} new length {len(self.specz)}")
                    # print(mask_dest)

                    # copy the required values into the output arrays
                    self.specz['LS_IDX'][mask_dest] = idxb[maskn]
                    self.specz['LS_ID'][mask_dest] = cat_ls['LS_ID'][idxb[maskn]]
                    self.specz['LS_RA'][mask_dest] = cat_ls['RA'][idxb[maskn]]
                    self.specz['LS_DEC'][mask_dest] = cat_ls['DEC'][idxb[maskn]]
                    self.specz['LS_PMRA'][mask_dest] = cat_ls['PMRA'][idxb[maskn]]
                    self.specz['LS_PMDEC'][mask_dest] = cat_ls['PMDEC'][idxb[maskn]]
                    self.specz['LS_EPOCH'][mask_dest] = cat_ls['REF_EPOCH'][idxb[maskn]]
                    self.specz['LS_MAG_G'][mask_dest] = nMgy2AB(cat_ls['FLUX_G'][idxb[maskn]])
                    self.specz['LS_MAG_R'][mask_dest] = nMgy2AB(cat_ls['FLUX_R'][idxb[maskn]])
                    self.specz['LS_MAG_Z'][mask_dest] = nMgy2AB(cat_ls['FLUX_Z'][idxb[maskn]])

                    self.specz['SPECZ_RAJ2000'][mask_dest] = coords_2000.ra.deg[idxb[maskn]]
                    self.specz['SPECZ_DEJ2000'][mask_dest] = coords_2000.dec.deg[idxb[maskn]]

                    self.specz['ORIG_LS_SEP'][mask_dest] = d2db[maskn].arcsec
                    self.specz['ORIG_LS_CTP_RANK'][mask_dest] = nn
                    self.specz['ORIG_LS_GT1CTP'][mask_dest] = True

                    masknn = np.pad(maskn, (0, ndups_tot))
                    self.specz['ORIG_LS_GT1CTP'][masknn] = True
                    for col in ['ORIG_CATCODE', 'ORIG_RA', 'ORIG_DEC', 'ORIG_POS_EPOCH',
                                'ORIG_ID_COL',
                                'ORIG_QUAL_COL', 'ORIG_REDSHIFT_COL', 'ORIG_CLASS_COL',
                                'ORIG_ID', 'ORIG_REDSHIFT', 'ORIG_QUAL', 'ORIG_NORMQ', 'ORIG_CLASS',
                                'ORIG_HASVI', 'ORIG_NORMC', 'ORIG_BITMASK']:
                        self.specz[col][mask_dest] = self.specz[col][masknn]


                    # print(self.specz[mask_dest])
                    # for idup,dup in enumerate(idxb[maskn])



            # print_comment(f"{self.name:8} with more than one ls ctp: {self.n_with_gt1ctp}")

            # self.specz['ORIG_LS_GT1CTP'] = self.with_gt1ctp
