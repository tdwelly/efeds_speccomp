# encoding: utf-8
#
# @Author: Tom Dwelly
# @Date: Nov-2021
# @Filename: speccat.py
# @License: BSD 3-Clause
# @Copyright: Tom Dwelly

from __future__ import division
from __future__ import print_function
from __future__ import absolute_import
from __future__ import unicode_literals

import os
import numpy as np
from efeds_speccomp.speccat import SpecCat, Epoch

'''
This module contains all of the settings for each input speccat, including the rules that
get us from the input spectral parameters to a normq
'''

def normq_gama(s):
    '''
    NQ>=4 is >=95% reliable, NQ=3 is >=90% reliable, and NQ<=2 is <90%
    reliable (see section 2.3.4 of
    [Liske et al (2015)|https://ui.adsabs.harvard.edu/abs/2015MNRAS.452.2087L/abstract|])
    '''
    normq = np.zeros(len(s), dtype=np.int32)
    normq[s['NQ'] == 2] = 1
    normq[s['NQ'] == 3] = 2
    normq[s['NQ'] >= 4] = 3
    return normq


def normq_wigglez(s):
    '''
    Q>=4 is better than 98.2% reliable, Q=3 is 84.2% reliable.
    See "Table 5. Redshift Quality Codes."
    of [Drinkwater et al (2018)|https://ui.adsabs.harvard.edu/abs/2018MNRAS.474.4151D/abstract]
    '''
    normq = np.zeros(len(s), dtype=np.int32)
    normq[s['Q'] == 2] = 1
    normq[s['Q'] == 3] = 2
    normq[s['Q'] >= 4] = 3
    return normq


def normq_2slaq(s):
    '''
    q_z2S==1 means "high-quality ID or redshift."
    q_z2S==2 means "poor-quality ID or redshift." - see
    [Croom et al 2009|https://ui.adsabs.harvard.edu/abs/2009MNRAS.392...19C/abstract]
    this explicitly excludes redshfits derived from SDSS spectroscopy (which have q_z2S=0)
    '''
    normq = np.zeros(len(s), dtype=np.int32)
    normq[s['q_z2S'] == 1] = 3
    normq[s['q_z2S'] == 2] = 2
    return normq

def normq_6dfgs(s):
    '''
    From vizier:
    Note (5)  : Quality value as follows:
    0 = 	no redshift quality value available
    1 = 	6dFGS or 2dFGRS, unusable measurement.
    2 = 	6dFGS or 2dFGRS, possible, but doubtful redshift estimate
    3 = 	6dFGS or 2dFGRS, 'probable' redshift (notionally 90% confidence)
    4 = 	6dFGS or 2dFGRS, reliable redshift (notionally 99% confidence)
    6 = 	6dFGS confirmed Galactic source
    9 = 	SDSS and ZCAT sources (not a measure of quality)

    See Jones et al. (2004MNRAS.355..747J) or Colless et al. (2001, Cat. VII/250, 2001MNRAS.328.1039C)
    for details on the quality schemes used by 6dFGS and 2dFGRS respectively).

    QUALITY == 3 means "6dFGS or 2dFGRS, reliable redshift (notionally 90% confidence)"
    QUALITY == 4 means "6dFGS or 2dFGRS, reliable redshift (notionally 99% confidence)"
    See table 6 of
    [Heath et al (2009)|https://ui.adsabs.harvard.edu/abs/2009MNRAS.399..683J/abstract].
    '''
    normq = np.zeros(len(s), dtype=np.int32)
    normq[s['q_cz'] == 6] = 3
    normq[s['q_cz'] == 4] = 3
    normq[s['q_cz'] == 3] = 2
    return normq

#
def normq_sdss_novi(s):
    '''
    These are based roughly on our SDSS VI experience
    '''
    normq = np.zeros(len(s), dtype=np.int32)
    m3 = np.where(
        (s['ZWARNING'] == 0) &
        (s['Z_ERR'] < 2e-3) &
        (s['SN_MEDIAN_ALL'] >= 2.0))
    m2 = np.where(
        (s['ZWARNING'] == 0) &
        (s['Z_ERR'] < 5e-3) &
        (s['SN_MEDIAN_ALL'] >= 1.0) &
        (s['SN_MEDIAN_ALL'] < 2.0))
    normq[m3] = 3
    normq[m2] = 2
    return normq

def normq_lamost(s):
    normq = np.zeros(len(s), dtype=np.int32)
    m3 = np.where(
        (s['snrr'] >= 10) &
        (s['z_err'] < 2e-3))
    m2 = np.where(
        (s['snrr'] >= 5) &
        (s['snrr'] < 10) &
        (s['z_err'] < 5e-3))
    normq[m3] = 3
    normq[m2] = 2
    return normq


def normq_simbad(s):
    normq = np.zeros(len(s), dtype=np.int32)
    m3 = np.where(
        (s['rvz_qual'] == 'A') |
        (s['rvz_qual'] == 'B') )
    m2 = np.where(
        (s['rvz_qual'] == 'C') |
        (s['rvz_qual'] == 'D') |
        (s['rvz_qual'] == 'E') )
    normq[m3] = 3
    normq[m2] = 2
    return normq


# ############
def setup_speccats(indir):
    efeds_vi_version = "2022-04-29T21-02-32-UTC"
    sdssv_vi_version = "2022-04-06T10-34-29-UTC"
    inspec = [
        SpecCat(
            name='sdssv_vi',
            release='pre-dr18',
            # filename='~/eFEDS/VI/sdssv_vi_data/2021-11-23T12-43-50-UTC/sdss_spec_results_2021-11-23T12-43-50-UTC.fits',
            filename=os.path.join(indir, f'SDSSV_VI/sdss_spec_results_{sdssv_vi_version}.fits'),
            col_ra='plug_ra',
            col_dec='plug_dec',
            col_z='best_sdss_z',
            col_class='best_sdss_class',
            col_id='fmc',
            col_qual='best_sdss_z_conf',
            criteria=[],  # 'best_sdss_z_conf !=1 ', ],
            match_radius=1.0,
            fiber_diam=2.0,
            epoch=Epoch(2021.2),
            ref='Merloni+, in prep',
            catalog_origin=f'http://erosita-bhm.mpe.mpg.de/sdssv_vi/data_dump/{sdssv_vi_version}/sdss_spec_results_{sdssv_vi_version}.fits',
            team_url='https://wiki.sdss.org/x/ZwCNBQ',
            normq='best_sdss_z_conf',
            notes='SDSSV eFEDS spectra, partially visually inspected by the SPIDERS team ',
            col_nvi="nvi",
            rank=1,
        ),
        SpecCat(
            name='boss_vi',
            release='dr16',
            # filename='~/eFEDS/VI/vi_data/2021-10-19T16-43-43-UTC/sdss_spec_results_2021-10-19T16-43-43-UTC.fits',
            filename=os.path.join(indir, f'SDSSIV_VI/sdss_spec_results_{efeds_vi_version}.fits'),
            col_ra='plug_ra',
            col_dec='plug_dec',
            col_z='best_sdss_z',
            col_class='best_sdss_class',
            col_id='pmf',
            col_qual='best_sdss_z_conf',
            criteria=['plate > 3500', 'plate < 12000'],
            match_radius=1.0,
            fiber_diam=2.0,
            epoch=Epoch(2011.2),
            ref='Merloni+, in prep',
            catalog_origin=f'http://erosita-bhm.mpe.mpg.de/efeds_vi/data_dump/{efeds_vi_version}/sdss_spec_results_{efeds_vi_version}.fits',
            team_url='https://trac.sdss.org/wiki/SPIDERS/eFEDS_VI',
            normq='best_sdss_z_conf',
            notes='SDSS DR16 (BOSS spectrograph) archival spectra visually inspected by the SPIDERS team ',
            col_nvi="nvi",
            rank=2,
        ),
        SpecCat(
            name='sdss_vi',
            release='dr16',
            # filename='~/eFEDS/VI/vi_data/2021-10-19T16-43-43-UTC/sdss_spec_results_2021-10-19T16-43-43-UTC.fits',
            filename=os.path.join(indir, f'SDSSIV_VI/sdss_spec_results_{efeds_vi_version}.fits'),
            col_ra='plug_ra',
            col_dec='plug_dec',
            col_z='best_sdss_z',
            col_class='best_sdss_class',
            col_id='pmf',
            col_qual='best_sdss_z_conf',
            criteria=['plate < 3500', ],
            match_radius=1.5,
            fiber_diam=3.0,
            epoch=Epoch(2003.0),
            ref='Merloni+, in prep',
            catalog_origin=f'http://erosita-bhm.mpe.mpg.de/efeds_vi/data_dump/{efeds_vi_version}/sdss_spec_results_{efeds_vi_version}.fits',
            team_url='https://trac.sdss.org/wiki/SPIDERS/eFEDS_VI',
            normq='best_sdss_z_conf',
            notes='SDSS DR16 (orig spectrograph) archival spectra visually inspected by the SPIDERS team ',
            col_nvi="nvi",
            rank=3,
        ),
        SpecCat(
            name='efeds_vi',
            release='pre-dr17',
            # filename='~/eFEDS/VI/vi_data/2021-10-19T16-43-43-UTC/sdss_spec_results_2021-10-19T16-43-43-UTC.fits',
            filename=os.path.join(indir, f'SDSSIV_VI/sdss_spec_results_{efeds_vi_version}.fits'),
            col_ra='plug_ra',
            col_dec='plug_dec',
            col_z='best_sdss_z',
            col_class='best_sdss_class',
            col_id='pmf',
            col_qual='best_sdss_z_conf',
            criteria=['plate >= 12525', ],
            match_radius=1.0,
            fiber_diam=2.0,
            epoch=Epoch(2020.2),
            ref='Merloni+, in prep',
            catalog_origin=f'http://erosita-bhm.mpe.mpg.de/efeds_vi/data_dump/{efeds_vi_version}/sdss_spec_results_{efeds_vi_version}.fits',
            team_url='https://trac.sdss.org/wiki/SPIDERS/eFEDS_VI',
            normq='best_sdss_z_conf',
            notes='SDSS-IV/eFEDS (March2020, DR17) spectra visually inspected by the SPIDERS team ',
            col_nvi="nvi",
            rank=4,
        ),
        SpecCat(
            name='gama',
            release='dr4',
            filename=os.path.join(indir, 'GAMA_DR4/SpecObj_eFEDS.fits'),
            col_ra='RA',
            col_dec='DEC',
            col_z='Z',
            col_class='const=GALAXY',
            col_id='GAMA_NAME',
            col_qual='NQ',
            criteria=['NQ>=2', ],
            match_radius=1.5,
            fiber_diam=2.1,
            epoch=Epoch(2000.0),
            ref='https://ui.adsabs.harvard.edu/abs/2022arXiv220308539D/abstract',
            catalog_origin='http://www.gama-survey.org/dr4/data/cat/SpecCat/v27/SpecObjv27.fits',
            team_url='http://www.gama-survey.org/dr4/',
            normq=normq_gama,
            notes=('NQ>=4 is >=95% reliable, NQ=3 is >=90% reliable, and NQ<=2 is <90% '
                   'reliable (see section 2.3.4 of '
                   '[Liske et al (2015)|https://ui.adsabs.harvard.edu/abs/2015MNRAS.452.2087L/abstract])'),
            rank=5,
        ),
        # SpecCat(
        #     name='gama',
        #     release='dr3',
        #     filename=os.path.join(indir, 'GAMA_DR3/SpecObj_eFEDS.fits'),
        #     col_ra='RA',
        #     col_dec='DEC',
        #     col_z='Z',
        #     col_class='const=GALAXY',
        #     col_id='GAMA_NAME',
        #     col_qual='NQ',
        #     criteria=['NQ>=2', ],
        #     match_radius=1.5,
        #     fiber_diam=2.1,
        #     epoch=Epoch(2000.0),
        #     ref='https://ui.adsabs.harvard.edu/abs/2018MNRAS.474.3875B/abstract',
        #     catalog_origin='http://www.gama-survey.org/dr3/data/cat/SpecCat/v27/SpecObj.fits',
        #     team_url='http://www.gama-survey.org/dr3/',
        #     normq=normq_gama,
        #     notes=('NQ>=4 is >=95% reliable, NQ=3 is >=90% reliable, and NQ<=2 is <90% '
        #            'reliable (see section 2.3.4 of '
        #            '[Liske et al (2015)|https://ui.adsabs.harvard.edu/abs/2015MNRAS.452.2087L/abstract])'),
        #     rank=5,
        # ),
        SpecCat(
            name='wigglez',
            release='fdr',
            # filename='~/erosita/eFEDS/WiggleZ/wigglez_eFEDS.fits',
            filename=os.path.join(indir, 'WiggleZ/wigglez_eFEDS.fits'),
            col_ra='RA',
            col_dec='Dec',
            col_z='redshift',
            col_class='class',
            col_id='WiggleZ_Name',
            col_qual='Q',
            criteria=['Q>=2', ],
            match_radius=1.5,
            fiber_diam=2.1,
            epoch=Epoch(2000.0),
            ref='https://ui.adsabs.harvard.edu/abs/2018MNRAS.474.4151D/abstract',
            catalog_origin='https://doi.org/10.1093/mnras/stx2963',
            team_url='http://wigglez.swin.edu.au/site/',
            normq=normq_wigglez,
            notes=('Q>=4 is better than 98.2% reliable, Q=3 is 84.2% reliable. '
                   'See "Table 5. Redshift Quality Codes." of '
                   '[Drinkwater et al (2018)|https://ui.adsabs.harvard.edu/abs/2018MNRAS.474.4151D/abstract]'),
            rank=6,
        ),
        SpecCat(
            name='2slaq',
            release='v1.2',
            # filename='~/erosita/eFEDS/2SLAQ/2SLAQ_Croom09_qsocat_eFEDS.fits',
            filename=os.path.join(indir, '2SLAQ/2SLAQ_Croom09_qsocat_eFEDS.fits'),
            col_ra='RAJ2000',
            col_dec='DEJ2000',
            col_z='z2S',
            col_class='const=QSO',
            col_id='name',
            criteria=['q_z2S>=1', 'q_z2S<=2'],
            col_qual='q_z2S',
            match_radius=1.5,
            fiber_diam=2.1,
            epoch=Epoch(2000.0),
            ref='https://ui.adsabs.harvard.edu/abs/2009MNRAS.392...19C/abstract',
            catalog_origin='http://vizier.u-strasbg.fr/viz-bin/VizieR-3?-source=J/MNRAS/392/19/2slaqqso',
            team_url='http://www.physics.usyd.edu.au/2slaq/',
            normq=normq_2slaq,
            notes=('q_z2S==1 means "high-quality redshift." '
                   'q_z2S==2 means "low-quality redshift." - see '
                   '[Croom et al (2009)|https://ui.adsabs.harvard.edu/abs/2009MNRAS.392...19C/abstract] '
                   '- this explicitly excludes redshfits derived from SDSS spectroscopy'),
            rank=7,
        ),
        SpecCat(
            name='6dFGS',
            release='fdr',
            # filename='~/erosita/SPIDERS/assorted/6dFGS/6dFGSzDR3_fromVizieR.fits',
            filename=os.path.join(indir, '6dFGS/6dFGSzDR3_fromVizieR.fits'),
            col_ra='RAJ2000',
            col_dec='DEJ2000',
            col_z='cz',
            col_z_is_cz=True,
            col_class='Template',
            col_id='_6dFGS',
            col_qual='q_cz',
            criteria=['q_cz >= 3',
                      'q_cz <= 6', ],
            match_radius=3.0,
            fiber_diam=6.7,
            epoch=Epoch(2000.0),
            ref='https://ui.adsabs.harvard.edu/abs/2009MNRAS.399..683J/abstract',
            catalog_origin='http://vizier.u-strasbg.fr/viz-bin/VizieR-3?-source=VII/259/6dfgs',
            team_url='http://www.6dfgs.net/',
            normq=normq_6dfgs,
            notes=(
                'QUALITY=3 means "6dFGS or 2dFGRS,reliable redshift (notionally 90% confidence)", '
                'QUALITY=4 means "6dFGS or 2dFGRS,reliable redshift (notionally 99% confidence)". '
                'See table 6 of '
                '[Heath et al (2009)|https://ui.adsabs.harvard.edu/abs/2009MNRAS.399..683J/abstract]. '
            ),
            rank=8,
        ),
        SpecCat(
            name='2mrs',
            release='v2.4',
            # filename='~/erosita/SPIDERS/assorted/2MRS/catalog/2mrs_1175_done.fits',
            filename=os.path.join(indir, '2MRS/2mrs_1175_done.fits'),
            col_ra='RA',
            col_dec='DEC',
            col_z='v',
            col_z_is_cz=True,
            col_class='TYPE',
            col_id='TMID',
            col_qual='FLAGS',
            criteria=[],
            match_radius=5.0,  # allow extended galaxies to be matched more easily
            fiber_diam=3.0,  # actually from a mix of instruments/observatories
            epoch=Epoch(2000.0),
            ref='https://ui.adsabs.harvard.edu/abs/2012ApJS..199...26H/abstract',
            catalog_origin='http://tdc-www.harvard.edu/2mrs/2mrs_v240.tgz',
            team_url='http://tdc-www.harvard.edu/2mrs/',
            normq=3,
            notes='No quality threshold applied (assume all reliable)',
            rank=9,
        ),
        SpecCat(
            name='boss_novi',
            release='dr16',
            # filename='~/erosita/eFEDS/SDSS_DR16/specObj-dr16_eFEDS.fits',
            filename=os.path.join(indir, 'SDSS_DR16/specObj-dr16_eFEDS.fits'),
            col_ra='PLUG_RA',
            col_dec='PLUG_DEC',
            col_z='Z',
            col_class='CLASS',
            col_id='pmf',
            col_qual='SN_MEDIAN_ALL',
            criteria=['PLATE > 3500',
                      'SPECPRIMARY == 1',
                      # 'ZWARNING == 0',
                      'Z_ERR > 0.0',
                      # 'Z_ERR < 2e-3',
                      'SN_MEDIAN_ALL > 1.0', ],
            match_radius=1.0,
            fiber_diam=2.0,
            epoch=Epoch(2000.0),
            ref='https://ui.adsabs.harvard.edu/abs/2020ApJS..249....3A/abstract',
            catalog_origin='https://data.sdss.org/sas/dr16/sdss/spectro/redux/specObj-dr16.fits',
            team_url='https://www.sdss.org/dr16',
            normq=normq_sdss_novi,
            notes='SDSS DR16 (BOSS spectrograph) spec-z (have not passed through the SPIDERS VI process)',
            rank=10,
        ),
        SpecCat(
            name='sdss_novi',
            release='dr16',
            # filename='~/erosita/eFEDS/SDSS_DR16/specObj-dr16_eFEDS.fits',
            filename=os.path.join(indir, 'SDSS_DR16/specObj-dr16_eFEDS.fits'),
            col_ra='PLUG_RA',
            col_dec='PLUG_DEC',
            col_z='Z',
            col_class='CLASS',
            col_id='pmf',
            col_qual='SN_MEDIAN_ALL',
            criteria=['PLATE < 3500',
                      'SPECPRIMARY == 1',
                      # 'ZWARNING == 0',
                      'Z_ERR > 0.0',
                      # 'Z_ERR < 2e-3',
                      'SN_MEDIAN_ALL>1.0', ],
            match_radius=1.5,
            fiber_diam=3.0,
            epoch=Epoch(2000.0),
            ref='https://ui.adsabs.harvard.edu/abs/2020ApJS..249....3A/abstract',
            catalog_origin='https://data.sdss.org/sas/dr16/sdss/spectro/redux/specObj-dr16.fits',
            team_url='https://www.sdss.org/dr16',
            normq=normq_sdss_novi,
            notes='SDSS DR16 (original spectrograph) spec-z (have not passed through the SPIDERS VI process)',
            rank=11,
        ),
        SpecCat(
            name='hectospec',
            release='RCSEDv2',
            # filename='~/erosita/eFEDS/RCSEDv2/RCSEDv2_HECTOSPEC_hectospec_eFEDS.fits',
            filename=os.path.join(indir, 'RCSEDv2/RCSEDv2_HECTOSPEC_hectospec_eFEDS.fits'),
            col_ra='ra',
            col_dec='dec',
            col_z='z',
            col_class='const=galaxy',
            col_id='spec',
            col_qual='snr_median',
            criteria=['snr_median>2.', ],
            match_radius=1.5,  #
            fiber_diam=1.5,  #
            epoch=Epoch(2000.0),
            ref='https://ui.adsabs.harvard.edu/abs/2017ApJS..228...14C/abstract',
            catalog_origin='https://gal-02.voxastro.org/rcsed2/hectospec/RESULT_TABLE_HECTOSPEC_hectospec_stellar_populations.fits',
            team_url='https://rcsed2.voxastro.org',
            normq=2,
            notes='',
            rank=12,
        ),
        SpecCat(
            name='fast',
            release='RCSEDv2',
            # filename='~/erosita/eFEDS/RCSEDv2/RCSEDv2_FAST_EMIS1_eFEDS.fits',
            filename=os.path.join(indir, 'RCSEDv2/RCSEDv2_FAST_EMIS1_eFEDS.fits'),
            col_ra='ra_j2000',
            col_dec='dec_j2000',
            col_z='z',
            col_class='const=galaxy',
            col_id='specid',
            col_qual='snr_median',
            criteria=['snr_median>2.', ],
            match_radius=3.0,  # allow a large search radius for these galaxies
            fiber_diam=1.0,  # unknown
            epoch=Epoch(2000.0),
            ref='https://ui.adsabs.harvard.edu/abs/2017ApJS..228...14C/abstract',
            catalog_origin='https://gal-02.voxastro.org/rcsed2/fast/RESULT_TABLE_FAST_EMIS1_stellar_populations.fits',
            team_url='https://rcsed2.voxastro.org',
            normq=2,
            notes='',
            rank=13,
        ),
        SpecCat(
            name='gaia_rvs',
            release='EDR3',
            # filename='~/eFEDS/Gaia_EDR3/gedr3_withRVS_eFEDS.fits',
            filename=os.path.join(indir, 'Gaia_EDR3/gedr3_withRVS_eFEDS.fits'),
            col_ra='RAJ2000',
            col_dec='DEJ2000',
            col_z='RVDR2',
            col_z_is_cz=True,
            col_class='const=STAR',
            col_id='Source',
            col_qual='e_RVDR2',
            criteria=[],
            match_radius=0.5,
            fiber_diam=0.5,  # TBD RVS effective aperture
            epoch=Epoch(2000.0),
            ref='https://ui.adsabs.harvard.edu/abs/2018A%26A...616A...6S/abstract',
            catalog_origin='http://vizier.u-strasbg.fr/viz-bin/VizieR-3?-source=I/350/gaiaedr3',
            team_url='https://www.cosmos.esa.int/web/gaia/earlydr3',
            normq=3,
            notes=(
                'No down-selection was applied to Gaia RVS measurements, '
                'since the released catalogue has already passed many quality checks, see '
                '[Sartoretti et al 2018|https://ui.adsabs.harvard.edu/abs/2018A%26A...616A...6S/abstract]',
            ),
            rank=14,
        ),
        SpecCat(
            name='LAMOST',
            release='DR7_v2.0',
            # filename='~/erosita/eFEDS/LAMOST/LAMOST_dr7_v1.3_eFEDS_unique.fits',
            filename=os.path.join(indir, 'LAMOST/LAMOST_dr7_v2.0_eFEDS_unique.fits'),
            col_ra='ra',
            col_dec='dec',
            col_z='z',
            col_class='class',
            col_id='designation',
            col_qual='snrr',
            criteria=['snrr>5.',
                      'z>=-1.0',
                      'z_err>=0.0'],
            match_radius=3.0,  # allow a large serach radius for these bright stars
            fiber_diam=3.3,
            epoch=Epoch(2000.0),
            ref='http://dr7.lamost.org/v2.0/doc/lr-data-production-description',
            catalog_origin='http://dr7.lamost.org/v2.0/catdl?name=dr7_v2.0_catalogue_LRS.fits.gz',
            team_url='http://www.lamost.org',
            normq=normq_lamost,
            notes=('Input catalogue filtered for uniqueness. '
                   'This quality selection gives 95% agreement (abs(deltaz)<1e-3) with SDSS DR16 '),
            rank=15,
        ),
        SpecCat(
            name='simbad',
            release='SIMBAD4 1.7-May-2018 on 2021.11.25 ',
            # filename='~/eFEDS/Simbad/Simbad_wspec_unique.fits',
            filename=os.path.join(indir, 'Simbad/Simbad_wspec_unique.fits'),
            col_ra='ra',
            col_dec='dec',
            col_z='rvz_redshift',
            col_class='otype_txt',
            col_id='main_id',
            col_qual='rvz_qual',
            criteria=[  # remove largest GAMA, SDSS DRx, SDSS DRxQ, and directly derived catalogues
                (
                    '(rvz_bibcode != "2011MNRAS.413..971D") & '  # GAMA DR1
                    '(rvz_bibcode != "2008ApJS..175..297A") & '  # SDSS DR6
                    '(rvz_bibcode != "2009ApJS..182..543A") & '  # SDSS DR7
                    '(rvz_bibcode != "2012ApJS..203...21A") & '  # SDSS DR9
                    '(rvz_bibcode != "2015ApJS..219...12A") & '  # SDSS DR11+12
                    '(rvz_bibcode != "2014A&A...563A..54P") & '  # SDSS DR10Q
                    '(rvz_bibcode != "2017A&A...597A..79P") & '  # SDSS DR12Q
                    '(rvz_bibcode != "2018A&A...613A..51P") & '  # SDSS DR14Q
                    '(rvz_bibcode != "2009MNRAS.395..255M") & '  # SDSS galaxy groups
                    '(rvz_bibcode != "2017A&A...597A..79P") & '  # SDSS compact galaxy groups
                    '(rvz_bibcode != "2016ApJS..224....1R")'     # REDMAPPER (spec-z from SDSS)
                ),
            ],
            match_radius=2.0,
            fiber_diam=2.0,  # actually from a wide mix of instruments
            epoch=Epoch(2000.0),
            ref='https://ui.adsabs.harvard.edu/abs/2000A%26AS..143....9W',
            catalog_origin='http://simbad.u-strasbg.fr:80/simbad/sim-tap',
            team_url='https://simbad.u-strasbg.fr/',
            normq=normq_simbad,
            notes=('Have applied a fairly arbitrary mapping of rvz_qual to normq. '
                   'Attempted to remove entries with spec-z from SDSS+GAMA '),
            rank=20,
        ),
        SpecCat(
            name='ned',
            release='NED on 2021.11.25 ',
            # filename='~/eFEDS/NED/NED_wspec_unique.fits',
            filename=os.path.join(indir, 'NED/NED_wspec_unique.fits'),
            col_ra='ra',
            col_dec='dec',
            col_z='z',
            col_class='pretype',
            col_id='prefname',
            col_qual='zunc',
            criteria=['zrefcode != "2016SDSSD.C...0000"', ],  # remove SDSS dr14
            match_radius=2.0,
            fiber_diam=2.0,  # actually a wide mix of instruments
            epoch=Epoch(2000.0),
            ref='https://ui.adsabs.harvard.edu/abs/2017IAUS..325..379M/abstract',
            catalog_origin='https://ned.ipac.caltech.edu/tap',
            team_url='https://ned.ipac.caltech.edu',
            normq=2,
            notes='No redshift quality threshold applied',
            rank=21,
        ),
    ]

    for sc_index, sc in enumerate(inspec):
        sc.set_bit(sc_index)

    return inspec
