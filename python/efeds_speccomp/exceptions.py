# !usr/bin/env python
# -*- coding: utf-8 -*-
#
# Licensed under a 3-clause BSD license.
#
# @Author: Brian Cherinka
# @Date:   2017-12-05 12:01:21
# @Last modified by:   Brian Cherinka
# @Last Modified time: 2017-12-05 12:19:32

from __future__ import print_function, division, absolute_import


class Efeds_speccompError(Exception):
    """A custom core Efeds_speccomp exception"""

    def __init__(self, message=None):

        message = 'There has been an error' \
            if not message else message

        super(Efeds_speccompError, self).__init__(message)


class Efeds_speccompNotImplemented(Efeds_speccompError):
    """A custom exception for not yet implemented features."""

    def __init__(self, message=None):

        message = 'This feature is not implemented yet.' \
            if not message else message

        super(Efeds_speccompNotImplemented, self).__init__(message)


class Efeds_speccompAPIError(Efeds_speccompError):
    """A custom exception for API errors"""

    def __init__(self, message=None):
        if not message:
            message = 'Error with Http Response from Efeds_speccomp API'
        else:
            message = 'Http response error from Efeds_speccomp API. {0}'.format(message)

        super(Efeds_speccompAPIError, self).__init__(message)


class Efeds_speccompApiAuthError(Efeds_speccompAPIError):
    """A custom exception for API authentication errors"""
    pass


class Efeds_speccompMissingDependency(Efeds_speccompError):
    """A custom exception for missing dependencies."""
    pass


class Efeds_speccompWarning(Warning):
    """Base warning for Efeds_speccomp."""


class Efeds_speccompUserWarning(UserWarning, Efeds_speccompWarning):
    """The primary warning class."""
    pass


class Efeds_speccompSkippedTestWarning(Efeds_speccompUserWarning):
    """A warning for when a test is skipped."""
    pass


class Efeds_speccompDeprecationWarning(Efeds_speccompUserWarning):
    """A warning for deprecated features."""
    pass
