#!/usr/bin/env python
# encoding: utf-8
#
# @Author: Tom Dwelly
# @Date: Oct-2019
# @Filename: eFEDS_target_wrangler
# @License: BSD 3-Clause
# @Copyright: Tom Dwelly

'''

New algorithm for v1.0.0+

 * read in all specz from each catalogue without quality filtering
 * gather likely counterparts to specz entries from lsdr8/lsdr9
 * associate specz with one or more legacysurvey objects (NWAYesque?)
   * take account of pms
   * allow for finite fiber size??
 * gather all specz for each ls_id
 * choose preferred z,class,normq for each ls_id
 * flag disagreements


'''

from __future__ import division
from __future__ import print_function
from __future__ import absolute_import
from __future__ import unicode_literals
import argparse

import os
import sys
import numpy as np
# from astropy.io import fits
import pymangle as mangle
# from astropy.time import Time
# from astropy.coordinates import SkyCoord, Distance
# from astropy import units as u
# from astropy.coordinates import search_around_sky
# from scipy.constants import speed_of_light
from tqdm import tqdm
from efeds_speccomp.print_func import print_comment, print_warning  # , print_error
from efeds_speccomp.speccat import load_ls, resolve, write_to_fits  # Epoch, SpecCat
from efeds_speccomp.setup_speccats import setup_speccats

# speed_of_light_kms = speed_of_light / 1000.  # km/s

__version__ = 'v1.4.3'

##

mangle_files = {
    'super_trim': os.path.expanduser('mangle/super-trim-efeds.ply'),
    'tight_trim': os.path.expanduser('mangle/trim-eboss28.ply'),
    'test_trim': os.path.expanduser('mangle/test-trim-efeds.ply'),
}

# he5ero_base = '/ero_archive5/erofollowup/Fields/eFEDS/specz_compilation'

#
if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog=os.path.basename(sys.argv[0]),
        description=('Builds a spectroscopic compilation catalogue '
                     'for the eFEDS field.')
    )

    parser.add_argument('-f', '--outfile',
                        required=True,
                        default=f'eFEDS_spectral_compilation_file_{__version__}.fits',
                        help='Where to write output')
    parser.add_argument('-i', '--indir', required=False, default='./inputs',
                        help='Base directory of inputs')
    parser.add_argument('-l', '--lsfile', required=False,
                        default='lsdr9/sweep_eFEDS_ls9_near_spec_unique.fits',
                        help='Where to find input legacysurvey catalogue')
    parser.add_argument('-d', '--debug', action='store_true', default=False,
                        help='sets debug (quick) mode')
    parser.add_argument('-v', '--verbose', action='store_true', default=False,
                        help='sets verbose mode')
    parser.add_argument('--version', action='version',
                        version=f'%(prog)s {__version__}')

    args = parser.parse_args()

    print_comment('Command line was: ',
                  os.path.basename(sys.argv[0]) + ' ' + ' '.join(sys.argv[1:]))

    mask_label = 'super_trim'
    if args.debug:
        mask_label = 'test_trim'
        print_warning("Running in debug mode (small sub-field)")

    outfile = os.path.expanduser(args.outfile)
    outdir = os.path.dirname(outfile)
    if outdir != '':
        os.makedirs(outdir, exist_ok=True)
        if os.path.isdir(outdir) is False:
            raise Exception(f"Error preparing output directory: {outdir}")

    indir = os.path.expanduser(args.indir)
    if os.path.isdir(indir) is False:
        raise Exception(f"Error finding input directory: {indir}")

    # open up the super_trim and trim mangles
    masks = {}
    for t, f in mangle_files.items():
        filename = os.path.join(indir, f)
        if os.path.isfile(filename) is False:
            raise Exception(f"Error finding {t} mangle: {filename}")
        try:
            masks[t] = mangle.Mangle(filename)
        except:
            raise Exception(f"Could not open {t} mangle: {filename}")

    # we will want to asscociate each spectrum with an ls9/ls8 counterpart, so load that in
    # ls_filename = os.path.expanduser(os.path.join(indir, args.lsfile))
    ls_filename = os.path.expanduser(args.lsfile)
    if os.path.isfile(ls_filename) is False:
        ls_filename = os.path.expanduser(os.path.join(indir, args.lsfile))

    cat_ls, coords_ls = load_ls(ls_filename=ls_filename,
                                mask=masks[mask_label])

    # setup all the catalogue parameters
    inspec = setup_speccats(indir)

    # load and prepare the input spectral catalogues
    # and do initial filtering
    catcode_rank = {}
    for c in inspec:
        c.prep(mask=masks[mask_label])
        c.match_to_ls(cat_ls=cat_ls, coords_ls=coords_ls)
        catcode_rank[c.name] = c.rank

    for c in inspec:
        print_comment(f"Bitmask entry: bit={c.bit:2} mask=0x{c.bitmask:08x} -> {c.name} ")

    nspec0 = np.sum([len(c.spectra) for c in inspec])
    print_comment("", f"Pre de-duplication, spectra list contains: {nspec0} rows")

    # combine all of the specz from all of the input catalogues
    specz = np.concatenate([c.specz for c in inspec])
    # order them by ls_id for convenience
    specz.sort(order=['LS_ID'])
    print_comment(f"There are {len(specz)} specz entries")

    # compute the histogram of orig_normq
    normq_p1_histo = np.bincount(specz['ORIG_NORMQ'] + 1, minlength=3)
    print_comment("Histogram of ORIG_NORMQ:")
    print_comment(f"#{'normq':7}  {'nspecz':8}")
    for i, ni in enumerate(normq_p1_histo):
        print_comment(f"{i-1:8d}  {ni:8d}")

    # now gather all of the ls_idxs
    ls_idx, ls_reverse, ls_idx_nrep = np.unique(
        # np.concatenate([c.ls_idx[c.with_ls] for c in inspec]),
        specz['LS_IDX'],
        return_counts=True,
        return_inverse=True)

    ls_idx_nrep_histo = np.bincount(np.extract(ls_idx >= 0, ls_idx_nrep))
    print_comment("", f"There are {len(ls_idx)} unique legacysurvey objects")
    print_comment("Histogram of multiplicity per ls_id:")
    print_comment(f"#{'nspec':7}  {'n_ls_idx':8}")
    for i, ni in enumerate(ls_idx_nrep_histo):
        print_comment(f"{i:8d}  {ni:8d}")

    print_comment("Computing final redshifts and calculating test statistics")
    for ii, idx in enumerate(tqdm(ls_idx)):
        ti = np.where(ls_reverse == ii)[0]  # selects specz rows matching this LS_IDX
        resolve(specz, idx, ti, catcode_rank)

    # make some more stats

    # compute the histogram of specz_orig
    m_best = np.where(specz['SPECZ_RANK'] == 1, True, False)
    u_catcode, n_catcode = np.unique(specz['SPECZ_CATCODE'][m_best], return_counts=True)
    print_comment("Number of times CATCODE is leading a specz:")
    print_comment(f"#{'catcode':11}  {'nspecz':8}")
    for a in zip(u_catcode, n_catcode):
        print_comment(f"{a[0]:12}  {a[1]:8d}")

    #write out the results to file
    write_to_fits(specz, inspec, outfile)
    print_comment(f"Wrote final output to file: {outfile}")

    print('## Input catalogue description table\n')
    s = ['CodeName', 'release',
         'Ntotal', 'Nfootprint', 'Nfiltered',
         # 'Nnew', 'Ndup',
         'Match radius (arcsec)',
         'Bits', 'Rank',
         'Quality Criteria',
         'File location',
         'Web', 'Main ref',
         'catalog origin', 'notes', ]
    print('|', ' | '.join(s), ' |')

    for c in inspec:
        s = [
            f"{c.name}",
            f"{c.release}",
            f"{c.initial_len}",
            f"{c.trim_len}",
            f"{c.n_filtered}",
            # f"{c.nnew}",
            # f"{c.ndup}",
            f"{c.match_radius}",
            f"{c.bit} -> {hex(c.bitmask)}",
            f"{c.rank}",
            f"{','.join(c.criteria)}",
            f"{c.filename.replace(indir,'./inputs')}",
            f"[website|{c.team_url}]",
            f"[ref|{c.ref}]",
            f"[cat|{c.catalog_origin}]",
            f"{c.notes}",
        ]
        print('|', " | ".join(s), ' |')

    print('## ORIG column mapping table\n')
    s = ['Codename', 'File location',
         'ORIG_ID column',
         'RA column',
         'DEC column',
         'REDSHIFT column',
         'ORIG_CLASS column',
         'ORIG_QUAL column',
         ]
    print('|', ' | '.join(s), ' |')

    for c in inspec:
        s = [
            f"{c.name}",
            f"{c.filename.replace(indir,'./inputs')}",
            f"{c.col_id}",
            f"{c.col_ra}",
            f"{c.col_dec}",
            f"{c.col_z}",
            f"{c.col_class}",
            f"{c.col_qual}",
        ]
        print('|', " | ".join(s), ' |')

    # ###
    exit(0)
