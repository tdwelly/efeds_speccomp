#!/usr/bin/env bash

######################################################################################

BASE=~/efeds_speccomp
cd ${BASE}

VERSION=v1.4.3



OUTDIR=${BASE}/${VERSION}
mkdir -p ${OUTDIR}
SPECCOMP=${OUTDIR}/spectra_compilation_eFEDS_${VERSION}.fits

## soft band catalogue
EDR=eFEDS_C001_Main_PointSources_CTP_redshift_V17.fits
# wget "https://erosita.mpe.mpg.de/edr/eROSITAObservations/Catalogues/salvatoM/${EDR}.gz"
# gunzip -v ${EDR}.gz
EDR_SPECZ=${OUTDIR}/${EDR%.*}_efeds_speccomp_${VERSION}.fits

# hard band catalogue
HEDR=eFEDS_C001_Hard_PointSources_CTP_redshift_V16.fits
HEDR_SPECZ=${OUTDIR}/${HEDR%.*}_efeds_speccomp_${VERSION}.fits

# Clusters catalogue
#CMEDR=~/eFEDS/CWG/eFEDS_clusters_V3.2_220427/eromapper_merged_eFEDS_clusters_V3.2_220427_legacy_only_catalog_members.fit
CMEDR=~/eFEDS/CWG/eFEDS_clusters_V3.2_220427/eromapper_merged_eFEDS_clusters_V3.2_220427_catalog_members.fit
CMEDR1=${CMEDR##*/}
CMEDR_SPECZ=${OUTDIR}/${CMEDR1%.*}_efeds_speccomp_${VERSION}.fits

STATSLOG=${SPECCOMP%.*}_stats.log
EFEDS_ALLSPEC=${BASE}/temp_allSDSS_spec.fits


###############################################
# RUN IT #

# ~/SDSSV/gitwork/bhm_targeting/misc/build_eFEDS_spectral_compilation.py -f ${SPECCOMP} | tee  ${SPECCOMP%.*}.log
# efeds_speccomp  -f ${SPECCOMP}  -i ${BASE}/inputs  | tee  ${SPECCOMP%.*}.log
efeds_speccomp  -f ${SPECCOMP}  -i ${BASE}/inputs -l  ${BASE}/inputs/lsdr9/sweep_eFEDS.fits | tee  ${SPECCOMP%.*}.log

ftcopy "${SPECCOMP}[1][SPECZ_RANK==1]" ${SPECCOMP%.*}_rank1.fits clobber=yes mode=q

# RUN IT #
###############################################

####only need to do this once:
if [ "true" = "ntrue" ]; then
  SPECOBJ_DR17=~/erosita/SPIDERS/SDSS/DR17/specObj-dr17.fits
  SPALL_DR18=~/SDSSV/sas/sdss5/bhm/boss/spectro/redux/v6_0_4/eFEDS/spAll-v6_0_4.fits
  stilts tcatn nin=2 in1=${SPECOBJ_DR17} in2=${SPALL_DR18} out=${EFEDS_ALLSPEC} ofmt=fits-basic \
       icmd1="select 'PLUG_RA>126&&PLUG_RA<146.2&&PLUG_DEC>-3.2&&PLUG_DEC<6.2'; addcol DR '\"DR17\"'; addcol -utype Integer FIELD '(int) 0'; addcol -utype Long CATALOGID '(long) 0'; \
keepcols 'PLUG_RA PLUG_DEC PLATE MJD FIBERID FIELD CATALOGID RUN2D DR'; \
addcol URL '\"https://dr17.sdss.org/sas/dr17/sdss/spectro/redux/\"+RUN2D+\"/spectra/lite/\"+padWithZeros(PLATE,4)+\"/spec-\"+padWithZeros(PLATE,4)+\"-\"+toString(MJD)+\"-\"+padWithZeros(FIBERID,4)+\".fits\"';" \
       icmd2="select 'PLUG_RA>126&&PLUG_RA<146.2&&PLUG_DEC>-3.2&&PLUG_DEC<6.2'; delcols 'PLATE'; addcol DR '\"DR18\"'; addcol -utype Integer PLATE '(int) toInteger(0)'; addcol -utype Integer FIBERID '(int) 0';  \
keepcols 'PLUG_RA PLUG_DEC PLATE MJD FIBERID FIELD CATALOGID RUN2D DR'; \
addcol URL '\"https://data.sdss5.org/sas/sdsswork/bhm/boss/spectro/redux/\"+ RUN2D +\"/eFEDS/spectra/lite/\"+padWithZeros(FIELD,5)+\"p/\"+toString(MJD)+\"/spec-\"+padWithZeros(FIELD,5)+\"-\"+toString(MJD)+\"-\"+padWithZeros(CATALOGID,11)+\".fits\"';"
fi

#######################
## matching to eFEDS X-ray catalogues
# https://erosita.mpe.mpg.de/edr/eROSITAObservations/Catalogues/

## soft band catalogue

stilts tmatch2 in1=$EDR in2=$SPECCOMP \
       out=$EDR_SPECZ \
       icmd2="select 'SPECZ_RANK==1';
addcol SPECZ_RA 'NULL_LS_RA ? SPECZ_RAJ2000 : LS_RA';
addcol SPECZ_DEC 'NULL_LS_DEC ? SPECZ_DEJ2000 : LS_DEC';" \
       matcher=sky \
       values1="CTP_LS8_RA CTP_LS8_DEC" params="1.0" \
       values2="SPECZ_RA SPECZ_DEC" \
       icmd1="" \
       fixcols=all suffix1= suffix2=_specz \
       find=best1 join=all1 \
       ofmt=fits-basic \
       ocmd="colmeta -name Separation_specz Separation;\
colmeta -name GroupID_specz GroupID;\
colmeta -name GroupSize_specz GroupSize;\
addcol has_specz '(NULL_SPECZ_CATCODE_specz?false:true)';\
addcol has_informative_specz 'NULL_SPECZ_REDSHIFT_specz==false&&SPECZ_NORMQ_specz>=2';\
tablename 'eFEDS_Main_ctp_wspecz';"


# now hard band catalogue
# wget "https://erosita.mpe.mpg.de/edr/eROSITAObservations/Catalogues/salvatoM/${HEDR}.gz"
# gunzip -v ${HEDR}.gz

stilts tmatch2 in1=$HEDR in2=$SPECCOMP \
       out=$HEDR_SPECZ \
       icmd2="select 'SPECZ_RANK==1';
addcol SPECZ_RA 'NULL_LS_RA ? SPECZ_RAJ2000 : LS_RA';
addcol SPECZ_DEC 'NULL_LS_DEC ? SPECZ_DEJ2000 : LS_DEC';" \
       matcher=sky \
       values1="CTP_LS8_RA CTP_LS8_DEC" params="1.0" \
       values2="SPECZ_RA SPECZ_DEC" \
       icmd1="" \
       fixcols=all suffix1= suffix2=_specz \
       find=best1 join=all1 \
       ofmt=fits-basic \
       ocmd="colmeta -name Separation_specz Separation;\
addcol has_specz '(NULL_SPECZ_CATCODE_specz?false:true)';\
addcol has_informative_specz 'NULL_SPECZ_REDSHIFT_specz==false&&SPECZ_NORMQ_specz>=2';\
tablename 'eFEDS_Hard_ctp_wspecz';"

# now cluster members catalogue
####### ln -s ~/eFEDS/CWG/eFEDS_clusters_V3.2_220427/eromapper_merged_eFEDS_clusters_V3.2_220427_legacy_only_catalog_members.fit ./


stilts tmatch2 in1=$CMEDR in2=$SPECCOMP \
       out=$CMEDR_SPECZ \
       icmd2="select 'SPECZ_RANK==1';
addcol SPECZ_RA 'NULL_LS_RA ? SPECZ_RAJ2000 : LS_RA';
addcol SPECZ_DEC 'NULL_LS_DEC ? SPECZ_DEJ2000 : LS_DEC';" \
       matcher=sky \
       values1="RA DEC" params="1.0" \
       values2="SPECZ_RA SPECZ_DEC" \
       icmd1="" \
       fixcols=all suffix1= suffix2=_specz \
       find=best1 join=all1 \
       ofmt=fits-basic \
       ocmd="colmeta -name Separation_specz Separation;\
addcol has_specz '(NULL_SPECZ_CATCODE_specz?false:true)';\
addcol has_informative_specz 'NULL_SPECZ_REDSHIFT_specz==false&&SPECZ_NORMQ_specz>=2';\
tablename 'eFEDS_Cluster_Members_wspecz';"

##########################
## compute some stats
echo "##########################################
Computing some stats"  | tee ${STATSLOG}

echo "
#############################
Speccomp catalogue stats" | tee -a ${STATSLOG}
echo "Number of input spec-z that meet initial minimum quality criteria: "`gawk '/Pre de-duplication, spectra list contains/ {nfm1=NF-1;print $nfm1}' ${SPECCOMP%.*}.log` | tee -a ${STATSLOG}
echo "Number of ls_id-spec-z combinations (this is the length of the full spectra_compilation_eFEDS_{version}.fits catalogue): "`nrows ${SPECCOMP}` | tee -a ${STATSLOG}
echo "Number of unique astrophysical objects with a spec-z (SPECZ_RANK==1): "`nrows "${SPECCOMP}[1][SPECZ_RANK==1]"` | tee -a ${STATSLOG}
echo "Number of unique legacysurvey dr9 objects with at least one spec-z ( SPECZ_RANK==1 && ORIG_LS_CTP_RANK==1): "`nrows "${SPECCOMP}[1][SPECZ_RANK==1&&ORIG_LS_CTP_RANK==1]"` | tee -a ${STATSLOG}
echo "Number of unique high quality spec-z ( SPECZ_RANK==1 && ORIG_LS_CTP_RANK==1 &&  SPECZ_NORMQ==3): "`nrows "${SPECCOMP}[1][SPECZ_RANK==1&&ORIG_LS_CTP_RANK==1&&SPECZ_NORMQ==3]"` | tee -a ${STATSLOG}

echo "
#############################
eFEDS Main catalogue stats" | tee -a ${STATSLOG}
echo "Number of objects in the eROSITA/eFEDS MAIN point source counterparts catalogue (v17): "`nrows "$EDR"` | tee -a ${STATSLOG}
echo "Number of objects in the eROSITA/eFEDS MAIN point source counterparts catalogue (v17) with at least one match in the spectra_compilation_eFEDS_{version}.fits catalogue (has_specz == True): "`nrows "${EDR_SPECZ}[1][has_specz]"` | tee -a ${STATSLOG}
echo "Number of objects in the eROSITA/eFEDS MAIN point source counterparts catalogue (v17) with at least one informative match in the spectra_compilation_eFEDS_{version}.fits catalogue (has_specz == True, SPECZ_NORMQ_specz>=2): "`nrows "${EDR_SPECZ}[1][has_specz&&SPECZ_NORMQ_specz>=2]"` | tee -a ${STATSLOG}
echo "Number of the above which are rated as being high-quality (SPECZ_NORMQ_specz == 3): "`nrows "${EDR_SPECZ}[1][has_specz&&SPECZ_NORMQ_specz==3]"` | tee -a ${STATSLOG}
qfstat "${EDR_SPECZ}[1][has_specz&&SPECZ_NORMQ_specz==3]" ORIG_CATCODE_specz | tee -a ${STATSLOG}

echo "
#############################
eFEDS Hard catalogue stats" | tee -a ${STATSLOG}
echo "Number of objects in the eROSITA/eFEDS HARD point source counterparts catalogue (v16): "`nrows "$HEDR"` | tee -a ${STATSLOG}
echo "Number of objects in the eROSITA/eFEDS HARD point source counterparts catalogue (v16) with at least one match in the spectra_compilation_eFEDS_{version}.fits catalogue (has_specz == True): "`nrows "${HEDR_SPECZ}[1][has_specz]"` | tee -a ${STATSLOG}
echo "Number of the above which are rated as being high-quality (SPECZ_NORMQ_specz == 3): "`nrows "${HEDR_SPECZ}[1][has_specz&&SPECZ_NORMQ_specz==3]"` | tee -a ${STATSLOG}
qfstat "${HEDR_SPECZ}[1][has_specz&&SPECZ_NORMQ_specz==3]" ORIG_CATCODE_specz | tee -a ${STATSLOG}


echo "
##########################################
"  | tee -a ${STATSLOG}

qfstat "${SPECCOMP}[1][]" ORIG_CATCODE | sort -k 1 > temp1
qfstat "${SPECCOMP}[1][SPECZ_RANK==1]" SPECZ_CATCODE | sort -k 1 > temp2
qfstat "${EDR_SPECZ}[1][has_specz]" SPECZ_CATCODE_specz | sort -k 1 > temp3
qfstat "${EDR_SPECZ}[1][has_informative_specz]" SPECZ_CATCODE_specz | sort -k 1 > temp4
qfstat "${EDR_SPECZ}[1][SPECZ_NORMQ_specz==3]" SPECZ_CATCODE_specz | sort -k 1 > temp5

echo "Number of spec-z contributed by each survey" | tee -a ${STATSLOG}
gawk 'BEGIN {print "| 0catcode|*Nspecz total|**Nspecz leading|***Nspecz leading eFEDS X-ray ctp|****Nspecz(informative) leading eFEDS X-ray ctp|****Nspecz(normq=3) leading eFEDS X-ray ctp|"} ARGIND==1 {n1[$1]=$2} ARGIND==2 {n2[$1]=$2}  ARGIND==3 {n3[$1]=$2} ARGIND==4 {n4[$1]=$2} ARGIND==5 {n5[$1]=$2} END {for(i in n1){printf("| %12s | %8d | %8d | %8d | %8d | %8d |\n", i, n1[i], n2[i], n3[i], n4[i], n5[i]); sum_n1+=n1[i]; sum_n2+=n2[i]; sum_n3+=n3[i]; sum_n4+=n4[i]; sum_n5+=n5[i]; }; printf("| %12s | %8d | %8d | %8d | %8d | %8d |\n", "ZTotal", sum_n1, sum_n2, sum_n3, sum_n4, sum_n5);}' temp1 temp2 temp3 temp4 temp5 | sort -k 2 | tee -a  ${STATSLOG}
rm temp1 temp2 temp3 temp4 temp5




echo "
##########################################
"  | tee -a ${STATSLOG}


stilts tmatch2 in1=${EDR_SPECZ} in2=${EFEDS_ALLSPEC} out=${EDR_SPECZ%.*}_simple_with_all_sdss_spec.fits matcher=sky \
       values1='CTP_LS8_RA CTP_LS8_DEC' values2='PLUG_RA PLUG_DEC' \
       icmd1='keepcols "ERO_Name ERO_ID_SRC CTP_LS8_UNIQUE_OBJID CTP_LS8_RA CTP_LS8_DEC SPECZ_N_specz SPECZ_REDSHIFT_specz SPECZ_NORMQ_specz SPECZ_NORMC_specz SPECZ_HASVI_specz SPECZ_CATCODE_specz SPECZ_BITMASK_specz";' \
       icmd2='keepcols "PLUG_RA PLUG_DEC PLATE MJD FIBERID FIELD CATALOGID RUN2D DR URL";' \
       params=1.0 join=1and2 find=all fixcols=all suffix1= suffix2= ofmt=fits-basic \
       ocmd="addcol NSPEC 'NULL_GroupSize?1:GroupSize'; \
delcols 'GroupID GroupSize'; \
colmeta -name Sep_LS8_SDSS Separation; \
sort ERO_ID_SRC;"

echo "SDSS spectra multiplicity stats: Main catalogue" | tee -a  ${STATSLOG}
qfstat ${EDR_SPECZ%.*}_simple_with_all_sdss_spec.fits ERO_ID_SRC fhisto | tee -a  ${STATSLOG}


stilts tmatch2 in1=${HEDR_SPECZ} in2=${EFEDS_ALLSPEC} out=${HEDR_SPECZ%.*}_simple_with_all_sdss_spec.fits matcher=sky \
       values1='CTP_LS8_RA CTP_LS8_DEC' values2='PLUG_RA PLUG_DEC' \
       icmd1='keepcols "ERO_Name ERO_ID_SRC CTP_LS8_UNIQUE_OBJID CTP_LS8_RA CTP_LS8_DEC SPECZ_N_specz SPECZ_REDSHIFT_specz SPECZ_NORMQ_specz SPECZ_NORMC_specz SPECZ_HASVI_specz SPECZ_CATCODE_specz SPECZ_BITMASK_specz";' \
       icmd2='keepcols "PLUG_RA PLUG_DEC PLATE MJD FIBERID FIELD CATALOGID RUN2D DR URL";' \
       params=1.0 join=1and2 find=all fixcols=all suffix1= suffix2= ofmt=fits-basic \
       ocmd="addcol NSPEC 'NULL_GroupSize?1:GroupSize'; \
delcols 'GroupID GroupSize'; \
colmeta -name Sep_LS8_SDSS Separation; \
sort ERO_ID_SRC;"

echo "SDSS spectra multiplicity stats: Hard catalogue" | tee -a  ${STATSLOG}
qfstat ${HEDR_SPECZ%.*}_simple_with_all_sdss_spec.fits ERO_ID_SRC fhisto | tee -a  ${STATSLOG}

#
stilts tcatn nin=2 in1="${EDR_SPECZ%.*}_simple_with_all_sdss_spec.fits" in2="${HEDR_SPECZ%.*}_simple_with_all_sdss_spec.fits" icmd1='addcol SAMPLE "\"MAIN\"";' icmd2='replacecol CTP_LS8_RA "(double)CTP_LS8_RA"; replacecol CTP_LS8_DEC "(double)CTP_LS8_DEC"; addcol SAMPLE "\"HARD\"";' out=${OUTDIR}/eFEDS_C001_MainAndHard_PointSources_CTP_redshift_V17andV16_efeds_speccomp_${VERSION}_simple_with_all_sdss_spec.fits ofmt=fits-basic




cat ${STATSLOG}


#### Prepare the VAC files ####
# See https://wiki.sdss.org/display/BHM/eFEDS+Spectroscopic+Redshift+Compilation#eFEDSSpectroscopicRedshiftCompilation-DatamodelforDR18VACs

# 9.2. eFEDS Main counterparts catalogue with specz+VI info
IN=${EDR_SPECZ}
OUT=${OUTDIR}/eFEDS_Main_speccomp-${VERSION}.fits

# only 67 characters allowed in TCOMM* keywords
#######################################################################################################################################

stilts tpipe \
       in=$IN out=$OUT ofmt=fits-basic \
       cmd='keepcols "ERO_Name ERO_ID_SRC ERO_RA_CORR ERO_DEC_CORR ERO_RADEC_ERR_CORR ERO_ML_FLUX ERO_ML_FLUX_ERR ERO_DET_LIKE CTP_LS8_UNIQUE_OBJID CTP_LS8_RA CTP_LS8_DEC Dist_CTP_LS8_ERO CTP_quality LS_ID_specz LS_RA_specz LS_DEC_specz LS_PMRA_specz LS_PMDEC_specz LS_EPOCH_specz LS_MAG_G_specz LS_MAG_R_specz LS_MAG_Z_specz SPECZ_N_specz SPECZ_RAJ2000_specz SPECZ_DEJ2000_specz SPECZ_NSEL_specz SPECZ_REDSHIFT_specz SPECZ_NORMQ_specz SPECZ_NORMC_specz SPECZ_HASVI_specz SPECZ_CATCODE_specz SPECZ_BITMASK_specz SPECZ_SEL_BITMASK_specz SPECZ_FLAGS_specz SPECZ_SEL_NORMQ_MAX_specz SPECZ_SEL_NORMQ_MEAN_specz SPECZ_SEL_Z_MEAN_specz SPECZ_SEL_Z_MEDIAN_specz SPECZ_SEL_Z_STDDEV_specz ORIG_RA_specz ORIG_DEC_specz ORIG_POS_EPOCH_specz ORIG_LS_SEP_specz ORIG_LS_GT1CTP_specz ORIG_LS_CTP_RANK_specz ORIG_ID_specz ORIG_REDSHIFT_specz ORIG_QUAL_specz ORIG_NORMQ_specz ORIG_CLASS_specz ORIG_HASVI_specz ORIG_NORMC_specz SPECZ_RA_specz SPECZ_DEC_specz Separation_specz has_specz has_informative_specz";
colmeta -name ERO_Name                -units ""             -desc "From Brunner+22, eROSITA official source Name"  ERO_Name                 ;
colmeta -name ERO_ID_SRC              -units ""             -desc "From Brunner+22, ID of eROSITA source in the Main Sample"  ERO_ID_SRC               ;
colmeta -name ERO_RA_CORR             -units "deg"          -desc "From Brunner+22, J2000 Right Ascension of eROSITA source (corrected)"  ERO_RA_CORR              ;
colmeta -name ERO_DEC_CORR            -units "deg"          -desc "From Brunner+22, J2000 Declination of eROSITA source (corrected)"  ERO_DEC_CORR             ;
colmeta -name ERO_RADEC_ERR_CORR      -units "arcsec"       -desc "From Brunner+22, eROSITA positional uncertainty (corrected)"  ERO_RADEC_ERR_CORR       ;
colmeta -name ERO_ML_FLUX             -units "erg/cm^2/s"   -desc "From Brunner+22, 0.2-2.3 keV source flux"  ERO_ML_FLUX              ;
colmeta -name ERO_ML_FLUX_ERR         -units "erg/cm^2/s"   -desc "From Brunner+22, 0.2-2.3 keV source flux error (1 sigma)"  ERO_ML_FLUX_ERR          ;
colmeta -name ERO_DET_LIKE            -units ""             -desc "From Brunner+22, 0.2-2.3 keV detection likelihood via PSF-fitting"  ERO_DET_LIKE             ;
colmeta -name CTP_LS8_UNIQUE_OBJID    -units ""             -desc "From Salvato+22, LS8 unique id for ctp to the eROSITA source"  CTP_LS8_UNIQUE_OBJID     ;
colmeta -name CTP_LS8_RA              -units "deg"          -desc "From Salvato+22, Right Ascension of the LS8 counterpart"  CTP_LS8_RA               ;
colmeta -name CTP_LS8_DEC             -units "deg"          -desc "From Salvato+22, Declination of the best LS8 counterpart"  CTP_LS8_DEC              ;
colmeta -name Dist_CTP_LS8_ERO        -units "arcsec"       -desc "From Salvato+22, Separation between ctp and eROSITA position"  Dist_CTP_LS8_ERO         ;
colmeta -name CTP_quality             -units ""             -desc "From Salvato+22, ctp qual: 4=best,3=good,2=secondary,1/0=unreliable"  CTP_quality              ;
colmeta -name LS_ID                   -units ""             -desc "Unique ID of lsdr9 photometric object labelled with spec-z"  LS_ID_specz              ;  
colmeta -name LS_RA                   -units "deg"          -desc "Coordinate from lsdr9 at epoch LS9_EPOCH"  LS_RA_specz              ;  
colmeta -name LS_DEC                  -units "deg"          -desc "Coordinate from lsdr9 at epoch LS9_EPOCH"  LS_DEC_specz             ;  
colmeta -name LS_PMRA                 -units "mas/yr"       -desc "Proper motion from lsdr9 "  LS_PMRA_specz            ;  
colmeta -name LS_PMDEC                -units "mas/yr"       -desc "Proper motion from lsdr9"  LS_PMDEC_specz           ;  
colmeta -name LS_EPOCH                -units "year"         -desc "Coordinate epoch from lsdr9"  LS_EPOCH_specz           ;  
colmeta -name LS_MAG_G                -units "mag"          -desc "DECam g-band model magnitude from lsdr9, AB"  LS_MAG_G_specz           ;  
colmeta -name LS_MAG_R                -units "mag"          -desc "DECam r-band model magnitude from lsdr9, AB"  LS_MAG_R_specz           ;  
colmeta -name LS_MAG_Z                -units "mag"          -desc "DECam z-band model magnitude from lsdr9, AB"  LS_MAG_Z_specz           ;  
colmeta -name SPECZ_N                 -units ""             -desc "Total number of spec-z associated with this lsdr9 object"  SPECZ_N_specz            ;  
colmeta -name SPECZ_RAJ2000           -units "deg"          -desc "Coordinate of spec-z, propagated if necessary to epoch J2000"  SPECZ_RAJ2000_specz      ;  
colmeta -name SPECZ_DEJ2000           -units "deg"          -desc "Coordinate of spec-z, propagated if necessary to epoch J2000"  SPECZ_DEJ2000_specz      ;  
colmeta -name SPECZ_NSEL              -units ""             -desc "Number of spec-z selected to inform result for this object"  SPECZ_NSEL_specz         ;  
colmeta -name SPECZ_REDSHIFT          -units ""             -desc "Final redshift determined for this object"  SPECZ_REDSHIFT_specz     ;  
colmeta -name SPECZ_NORMQ             -units ""             -desc "Final normalised redshift quality associated with this object"  SPECZ_NORMQ_specz        ;  
colmeta -name SPECZ_NORMC             -units ""             -desc "Final normlised classfication determined for this object"  SPECZ_NORMC_specz        ;  
colmeta -name SPECZ_HASVI             -units ""             -desc "True if best spec-z for this object has a visual inspection"  SPECZ_HASVI_specz        ;  
colmeta -name SPECZ_CATCODE           -units ""             -desc "Catalogue code of best spec-z for this object"  SPECZ_CATCODE_specz      ;  
colmeta -name SPECZ_BITMASK           -units ""             -desc "Bitmask encoding catalogues containing spec-z for this object"  SPECZ_BITMASK_specz      ;  
colmeta -name SPECZ_SEL_BITMASK       -units ""             -desc "Bitmask encoding catalogues containing informative spec-z for object"  SPECZ_SEL_BITMASK_specz  ;  
colmeta -name SPECZ_FLAGS             -units ""             -desc "Bitmask encoding quality flags for this object"  SPECZ_FLAGS_specz        ;  
colmeta -name SPECZ_SEL_NORMQ_MAX     -units ""             -desc "Highest NORMQ of informative spec-z for this object"  SPECZ_SEL_NORMQ_MAX_specz;  
colmeta -name SPECZ_SEL_NORMQ_MEAN    -units ""             -desc "Mean NORMQ of informative spec-z for this object"  SPECZ_SEL_NORMQ_MEAN_specz;  
colmeta -name SPECZ_SEL_Z_MEAN        -units ""             -desc "Mean REDSHIFT of informative spec-z for this object"  SPECZ_SEL_Z_MEAN_specz   ;  
colmeta -name SPECZ_SEL_Z_MEDIAN      -units ""             -desc "Median REDSHIFT of informative spec-z for this object"  SPECZ_SEL_Z_MEDIAN_specz ;  
colmeta -name SPECZ_SEL_Z_STDDEV      -units ""             -desc "Standard deviation of REDSHIFTs for informative spec-z for object"  SPECZ_SEL_Z_STDDEV_specz ;       
colmeta -name SPECZ_ORIG_RA           -units "deg"          -desc "Coordinate associated with individual spec-z measurement"  ORIG_RA_specz            ;  
colmeta -name SPECZ_ORIG_DEC          -units "deg"          -desc "Coordinate associated with individual spec-z measurement"  ORIG_DEC_specz           ;  
colmeta -name SPECZ_ORIG_POS_EPOCH    -units ""             -desc "Coordinate epoch associated with individual spec-z measurement"  ORIG_POS_EPOCH_specz     ;  
colmeta -name SPECZ_ORIG_LS_SEP       -units "arcsec"       -desc "Distance from spec-z to lsdr9 photometric ctp (corrected for pm)"  ORIG_LS_SEP_specz        ;  
colmeta -name SPECZ_ORIG_LS_GT1CTP    -units ""             -desc "Can spec-z be associated with >1 possible lsdr9 counterpart?"  ORIG_LS_GT1CTP_specz     ;  
colmeta -name SPECZ_ORIG_LS_CTP_RANK  -units ""             -desc "Rank of ctp out of all possibilities for this spec-z (1=closest)"  ORIG_LS_CTP_RANK_specz   ;  
colmeta -name SPECZ_ORIG_ID           -units ""             -desc "Orig. value of ID of individual spec-z measurement (as a string)"  ORIG_ID_specz            ;  
colmeta -name SPECZ_ORIG_REDSHIFT     -units ""             -desc "Orig. redshift value of individual spec-z measurement"  ORIG_REDSHIFT_specz      ;  
colmeta -name SPECZ_ORIG_QUAL         -units ""             -desc "Orig. redshift quality value of individual spec-z measurement"  ORIG_QUAL_specz          ;  
colmeta -name SPECZ_ORIG_NORMQ        -units ""             -desc "Orig. redshift quality of individual spec-z measurement - normalised"  ORIG_NORMQ_specz         ;  
colmeta -name SPECZ_ORIG_CLASS        -units ""             -desc "Orig. classification label of individual spec-z measurement"  ORIG_CLASS_specz         ;  
colmeta -name SPECZ_ORIG_HASVI        -units ""             -desc "True if individual spec-z has a visual inspection from our team"  ORIG_HASVI_specz         ;  
colmeta -name SPECZ_ORIG_NORMC        -units ""             -desc "Normalised classification code of individual spec-z measurement"  ORIG_NORMC_specz         ;  
colmeta -name SPECZ_RA_USED           -units "deg"          -desc "Adopted coordinate of specz when matching to Salvato+22 counterpart"  SPECZ_RA_specz           ;  
colmeta -name SPECZ_DEC_USED          -units "deg"          -desc "Adopted coordinate of specz when matching to Salvato+22 counterpart"  SPECZ_DEC_specz          ;  
colmeta -name Separation_SPECZ_CTP    -units "arcsec"       -desc "Dist. from CTP_LS8_RA,CTP_LS8_DEC to SPECZ_RA_specz,SPECZ_DEC_specz"  Separation_specz         ;
colmeta -name has_specz               -units ""             -desc "Does this Salvato+22 counterpart have a spec-z?"  has_specz                ;
colmeta -name has_informative_specz   -units ""             -desc "Does this Salvato+22 counterpart have an informative spec-z?"  has_informative_specz    ;'

# only 67 characters allowed in TCOMM* keywords
#######################################################################################################################################

# 9.3. eFEDS Hard counterparts catalogue with specz+VI info
IN=${HEDR_SPECZ}
OUT=${OUTDIR}/eFEDS_Hard_speccomp-${VERSION}.fits

# only 67 characters allowed in TCOMM* keywords
#######################################################################################################################################
stilts tpipe \
       in=$IN out=$OUT ofmt=fits-basic \
       cmd='keepcols "ERO_Name ERO_ID_SRC ERO_RA_CORR ERO_DEC_CORR ERO_RADEC_ERROR_CORR ERO_ML_FLUX_3 ERO_ML_FLUX_ERR_3 ERO_DET_LIKE_3 CTP_LS8_UNIQUE_OBJID CTP_LS8_RA CTP_LS8_DEC Dist_CTP_LS8_ERO CTP_quality LS_ID_specz LS_RA_specz LS_DEC_specz LS_PMRA_specz LS_PMDEC_specz LS_EPOCH_specz LS_MAG_G_specz LS_MAG_R_specz LS_MAG_Z_specz SPECZ_N_specz SPECZ_RAJ2000_specz SPECZ_DEJ2000_specz SPECZ_NSEL_specz SPECZ_REDSHIFT_specz SPECZ_NORMQ_specz SPECZ_NORMC_specz SPECZ_HASVI_specz SPECZ_CATCODE_specz SPECZ_BITMASK_specz SPECZ_SEL_BITMASK_specz SPECZ_FLAGS_specz SPECZ_SEL_NORMQ_MAX_specz SPECZ_SEL_NORMQ_MEAN_specz SPECZ_SEL_Z_MEAN_specz SPECZ_SEL_Z_MEDIAN_specz SPECZ_SEL_Z_STDDEV_specz ORIG_RA_specz ORIG_DEC_specz ORIG_POS_EPOCH_specz ORIG_LS_SEP_specz ORIG_LS_GT1CTP_specz ORIG_LS_CTP_RANK_specz ORIG_ID_specz ORIG_REDSHIFT_specz ORIG_QUAL_specz ORIG_NORMQ_specz ORIG_CLASS_specz ORIG_HASVI_specz ORIG_NORMC_specz SPECZ_RA_specz SPECZ_DEC_specz Separation_specz has_specz has_informative_specz";
colmeta -name ERO_Name                -units ""             -desc "From Brunner+22, eROSITA official source Name"  ERO_Name                 ;
colmeta -name ERO_ID_SRC              -units ""             -desc "From Brunner+22, ID of eROSITA source in the Main Sample"  ERO_ID_SRC               ;
colmeta -name ERO_RA_CORR             -units "deg"          -desc "From Brunner+22, J2000 Right Ascension of eROSITA source (corrected)"  ERO_RA_CORR              ;
colmeta -name ERO_DEC_CORR            -units "deg"          -desc "From Brunner+22, J2000 Declination of eROSITA source (corrected)"  ERO_DEC_CORR             ;
colmeta -name ERO_RADEC_ERR_CORR      -units "arcsec"       -desc "From Brunner+22, eROSITA positional uncertainty (corrected)"  ERO_RADEC_ERROR_CORR       ;
colmeta -name ERO_ML_FLUX_3           -units "erg/cm^2/s"   -desc "From Brunner+22, 2.3-5.0 keV source flux"  ERO_ML_FLUX_3              ;
colmeta -name ERO_ML_FLUX_ERR_3       -units "erg/cm^2/s"   -desc "From Brunner+22, 2.3-5.0 keV source flux error (1 sigma)"  ERO_ML_FLUX_ERR_3         ;
colmeta -name ERO_DET_LIKE_3          -units ""             -desc "From Brunner+22, 2.3-5.0 keV detection likelihood via PSF-fitting"  ERO_DET_LIKE_3             ;
colmeta -name CTP_LS8_UNIQUE_OBJID    -units ""             -desc "From Salvato+22, LS8 unique id for ctp to the eROSITA source"  CTP_LS8_UNIQUE_OBJID     ;
colmeta -name CTP_LS8_RA              -units "deg"          -desc "From Salvato+22, Right Ascension of the LS8 counterpart"  CTP_LS8_RA               ;
colmeta -name CTP_LS8_DEC             -units "deg"          -desc "From Salvato+22, Declination of the best LS8 counterpart"  CTP_LS8_DEC              ;
colmeta -name Dist_CTP_LS8_ERO        -units "arcsec"       -desc "From Salvato+22, Separation between ctp and eROSITA position"  Dist_CTP_LS8_ERO         ;
colmeta -name CTP_quality             -units ""             -desc "From Salvato+22, ctp qual: 4=best,3=good,2=secondary,1/0=unreliable"  CTP_quality              ;
colmeta -name LS_ID                   -units ""             -desc "Unique ID of lsdr9 photometric object labelled with spec-z"  LS_ID_specz              ;  
colmeta -name LS_RA                   -units "deg"          -desc "Coordinate from lsdr9 at epoch LS9_EPOCH"  LS_RA_specz              ;  
colmeta -name LS_DEC                  -units "deg"          -desc "Coordinate from lsdr9 at epoch LS9_EPOCH"  LS_DEC_specz             ;  
colmeta -name LS_PMRA                 -units "mas/yr"       -desc "Proper motion from lsdr9 "  LS_PMRA_specz            ;  
colmeta -name LS_PMDEC                -units "mas/yr"       -desc "Proper motion from lsdr9"  LS_PMDEC_specz           ;  
colmeta -name LS_EPOCH                -units "year"         -desc "Coordinate epoch from lsdr9"  LS_EPOCH_specz           ;  
colmeta -name LS_MAG_G                -units "mag"          -desc "DECam g-band model magnitude from lsdr9, AB"  LS_MAG_G_specz           ;  
colmeta -name LS_MAG_R                -units "mag"          -desc "DECam r-band model magnitude from lsdr9, AB"  LS_MAG_R_specz           ;  
colmeta -name LS_MAG_Z                -units "mag"          -desc "DECam z-band model magnitude from lsdr9, AB"  LS_MAG_Z_specz           ;  
colmeta -name SPECZ_N                 -units ""             -desc "Total number of spec-z associated with this lsdr9 object"  SPECZ_N_specz            ;  
colmeta -name SPECZ_RAJ2000           -units "deg"          -desc "Coordinate of spec-z, propagated if necessary to epoch J2000"  SPECZ_RAJ2000_specz      ;  
colmeta -name SPECZ_DEJ2000           -units "deg"          -desc "Coordinate of spec-z, propagated if necessary to epoch J2000"  SPECZ_DEJ2000_specz      ;  
colmeta -name SPECZ_NSEL              -units ""             -desc "Number of spec-z selected to inform result for this object"  SPECZ_NSEL_specz         ;  
colmeta -name SPECZ_REDSHIFT          -units ""             -desc "Final redshift determined for this object"  SPECZ_REDSHIFT_specz     ;  
colmeta -name SPECZ_NORMQ             -units ""             -desc "Final normalised redshift quality associated with this object"  SPECZ_NORMQ_specz        ;  
colmeta -name SPECZ_NORMC             -units ""             -desc "Final normlised classfication determined for this object"  SPECZ_NORMC_specz        ;  
colmeta -name SPECZ_HASVI             -units ""             -desc "True if best spec-z for this object has a visual inspection"  SPECZ_HASVI_specz        ;  
colmeta -name SPECZ_CATCODE           -units ""             -desc "Catalogue code of best spec-z for this object"  SPECZ_CATCODE_specz      ;  
colmeta -name SPECZ_BITMASK           -units ""             -desc "Bitmask encoding catalogues containing spec-z for this object"  SPECZ_BITMASK_specz      ;  
colmeta -name SPECZ_SEL_BITMASK       -units ""             -desc "Bitmask encoding catalogues containing informative spec-z for object"  SPECZ_SEL_BITMASK_specz  ;  
colmeta -name SPECZ_FLAGS             -units ""             -desc "Bitmask encoding quality flags for this object"  SPECZ_FLAGS_specz        ;  
colmeta -name SPECZ_SEL_NORMQ_MAX     -units ""             -desc "Highest NORMQ of informative spec-z for this object"  SPECZ_SEL_NORMQ_MAX_specz;  
colmeta -name SPECZ_SEL_NORMQ_MEAN    -units ""             -desc "Mean NORMQ of informative spec-z for this object"  SPECZ_SEL_NORMQ_MEAN_specz;  
colmeta -name SPECZ_SEL_Z_MEAN        -units ""             -desc "Mean REDSHIFT of informative spec-z for this object"  SPECZ_SEL_Z_MEAN_specz   ;  
colmeta -name SPECZ_SEL_Z_MEDIAN      -units ""             -desc "Median REDSHIFT of informative spec-z for this object"  SPECZ_SEL_Z_MEDIAN_specz ;  
colmeta -name SPECZ_SEL_Z_STDDEV      -units ""             -desc "Standard deviation of REDSHIFTs for informative spec-z for object"  SPECZ_SEL_Z_STDDEV_specz ;       
colmeta -name SPECZ_ORIG_RA           -units "deg"          -desc "Coordinate associated with individual spec-z measurement"  ORIG_RA_specz            ;  
colmeta -name SPECZ_ORIG_DEC          -units "deg"          -desc "Coordinate associated with individual spec-z measurement"  ORIG_DEC_specz           ;  
colmeta -name SPECZ_ORIG_POS_EPOCH    -units ""             -desc "Coordinate epoch associated with individual spec-z measurement"  ORIG_POS_EPOCH_specz     ;  
colmeta -name SPECZ_ORIG_LS_SEP       -units "arcsec"       -desc "Distance from spec-z to lsdr9 photometric ctp (corrected for pm)"  ORIG_LS_SEP_specz        ;  
colmeta -name SPECZ_ORIG_LS_GT1CTP    -units ""             -desc "Can spec-z be associated with >1 possible lsdr9 counterpart?"  ORIG_LS_GT1CTP_specz     ;  
colmeta -name SPECZ_ORIG_LS_CTP_RANK  -units ""             -desc "Rank of ctp out of all possibilities for this spec-z (1=closest)"  ORIG_LS_CTP_RANK_specz   ;  
colmeta -name SPECZ_ORIG_ID           -units ""             -desc "Orig. value of ID of individual spec-z measurement (as a string)"  ORIG_ID_specz            ;  
colmeta -name SPECZ_ORIG_REDSHIFT     -units ""             -desc "Orig. redshift value of individual spec-z measurement"  ORIG_REDSHIFT_specz      ;  
colmeta -name SPECZ_ORIG_QUAL         -units ""             -desc "Orig. redshift quality value of individual spec-z measurement"  ORIG_QUAL_specz          ;  
colmeta -name SPECZ_ORIG_NORMQ        -units ""             -desc "Orig. redshift quality of individual spec-z measurement - normalised"  ORIG_NORMQ_specz         ;  
colmeta -name SPECZ_ORIG_CLASS        -units ""             -desc "Orig. classification label of individual spec-z measurement"  ORIG_CLASS_specz         ;  
colmeta -name SPECZ_ORIG_HASVI        -units ""             -desc "True if individual spec-z has a visual inspection from our team"  ORIG_HASVI_specz         ;  
colmeta -name SPECZ_ORIG_NORMC        -units ""             -desc "Normalised classification code of individual spec-z measurement"  ORIG_NORMC_specz         ;  
colmeta -name SPECZ_RA_USED           -units "deg"          -desc "Adopted coordinate of specz when matching to Salvato+22 counterpart"  SPECZ_RA_specz           ;  
colmeta -name SPECZ_DEC_USED          -units "deg"          -desc "Adopted coordinate of specz when matching to Salvato+22 counterpart"  SPECZ_DEC_specz          ;  
colmeta -name Separation_SPECZ_CTP    -units "arcsec"       -desc "Dist. from CTP_LS8_RA,CTP_LS8_DEC to SPECZ_RA_specz,SPECZ_DEC_specz"  Separation_specz         ;
colmeta -name has_specz               -units ""             -desc "Does this Salvato+22 counterpart have a spec-z?"  has_specz                ;
colmeta -name has_informative_specz   -units ""             -desc "Does this Salvato+22 counterpart have an informative spec-z?"  has_informative_specz    ;'
# only 67 characters allowed in TCOMM* keywords
#######################################################################################################################################


##################

# 9.4. eFEDS additional targets+specz+VI info
IN=${BASE}/inputs/SDSSV_VI/sdss_spec_results_2022-04-06T10-34-29-UTC.fits
OUT=${OUTDIR}/eFEDS_SDSSV_spec_results-${VERSION}.fits

# only 67 characters allowed in TCOMM* keywords
##########################################################################################################################
stilts tpipe \
       in=$IN out=$OUT ofmt=fits-basic \
       cmd='keepcols "field mjd catalogid plug_ra plug_dec nvi sn_median_all z_pipe z_err_pipe zwarning_pipe class_pipe subclass_pipe best_sdss_z best_sdss_z_conf best_sdss_class blazar_candidate";
colmeta -name FIELD            -units ""       -desc "SDSS field code identifier" field           ;    
colmeta -name MJD              -units ""       -desc "SDSS MJD associated with this spectrum" mjd             ;
colmeta -name CATALOGID        -units ""       -desc "SDSS-V CATALOGID (v0) associated with this target" catalogid         ;
colmeta -name PLUG_RA          -units "deg"    -desc "Sky coordinate of spectroscopic fiber" plug_ra         ;
colmeta -name PLUG_DEC         -units "deg"    -desc "Sky coordinate of spectroscopic fiber" plug_dec        ;
colmeta -name NVI              -units ""       -desc "Number of visual inspections collected for this spectrum" nvi             ;
colmeta -name SN_MEDIAN_ALL    -units ""       -desc "Median SNR/pix in spectrum (idlspec2d eFEDS v6_0_2 reductions)" sn_median_all   ;
colmeta -name Z_PIPE           -units ""       -desc "Pipeline redshift in idlspec1d eFEDS v6_0_2 reductions" z_pipe          ;
colmeta -name Z_ERR_PIPE       -units ""       -desc "Pipeline redshift uncertainty in idlspec1d eFEDS v6_0_2 reductions" z_err_pipe      ;
colmeta -name ZWARNING_PIPE    -units ""       -desc "Pipeline redshift warning flags in idlspec1d eFEDS v6_0_2 reductions" zwarning_pipe   ;
colmeta -name CLASS_PIPE       -units ""       -desc "Pipeline classification in idlspec1d eFEDS v6_0_2 reductions" class_pipe      ;
colmeta -name SUBCLASS_PIPE    -units ""       -desc "Pipeline sub-classification in idlspec1d eFEDS v6_0_2 reductions" subclass_pipe   ;
colmeta -name Z_FINAL          -units ""       -desc "Final redshift derived from pipeline and visual inspections" best_sdss_z     ;
colmeta -name Z_CONF_FINAL     -units ""       -desc "Final redshift confidence from pipeline and visual inspections" best_sdss_z_conf; 
colmeta -name CLASS_FINAL      -units ""       -desc "Final classfication derived from pipeline and visual inspections" best_sdss_class ;
colmeta -name BLAZAR_CANDIDATE -units ""       -desc "Was object flagged as a blazar candidate in visual inspections?" blazar_candidate;'

##########################################################################################################################


# make some text files to carry the column descriptions
#IN=${OUTDIR}/eFEDS_SDSSV_spec_results-${VERSION}.fits
#ftlist ${IN}+1 K mode=q 

for F in ${OUTDIR}/eFEDS_Main_speccomp-${VERSION}.fits ${OUTDIR}/eFEDS_Hard_speccomp-${VERSION}.fits ${OUTDIR}/eFEDS_SDSSV_spec_results-${VERSION}.fits; do
    META=${F%.*}_description.txt
    stilts tpipe in=$F out=- ofmt="text(maxCell=100)" cmd='meta Name Units Description' | gawk --field-separator='|' 'BEGIN {printf("Summary\n-------\n\n\nColumns\n-------\n\n")} $0~/^|/{if(NF>=4&&$2!~/ Name /){gsub(/[ \t]+$/,"",$2);gsub(/^[ \t]+/,"",$2);gsub(/[ \t]+$/,"",$3);gsub(/^[ \t]+/,"",$3);gsub(/[ \t]+$/,"",$4);gsub(/^[ \t]+/,"",$4);printf("%s - %s [%s]\n", $2,$4,$3)};n++} END {print _}' > $META
done

exit
########




##extras
qfstat "${CMEDR_SPECZ}[1][has_specz]" ORIG_CATCODE_specz
qfstat "${CMEDR_SPECZ}[1][has_specz&&cg]" ORIG_CATCODE_specz
qfstat "${CMEDR_SPECZ}[1][has_specz&&cg&&(!(SPECZ_HASVI_specz))]" ORIG_CATCODE_specz
# gump "${CMEDR_SPECZ}[1][has_specz&&strstr(ORIG_CATCODE_specz,'novi')>0]" "ORIG_ID_specz" no  > plate_mjd_fiberid_list.txt


# make a wiki table with column names, format, units
ftlist ${EDR_SPECZ}+1 C mode=q | gawk 'BEGIN {flag=0; printf("||Name(current)||Name(release)||Format||Unit||Keep?||Description||\n")} flag==1 {u=(NF>7?$4:" "); gsub("\\[","",u);gsub("]","",u); printf("|%s|%s|%s|%s| | |\n", $2, $2, $3, u)} $1=="Col" {flag=1}' > cols_for_wiki.txt


ftlist inputs/SDSSV_VI/sdss_spec_results_2022-04-06T10-34-29-UTC.fits+1 C mode=q | gawk 'BEGIN {flag=0; printf("||Name(current)||Name(release)||Format||Unit||Keep?||Description||\n")} flag==1 {u=(NF>7?$4:" "); gsub("\\[","",u);gsub("]","",u); printf("|%s|%s|%s|%s|yes| |\n", $2, $2, $3, u)} $1=="Col" {flag=1}'

> cols_for_wiki2.txt



