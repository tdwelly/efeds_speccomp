.. _efeds_speccomp-changelog:

==========
Change Log
==========

* v1.4.2 - Update SDSS-IV + SDSSV VI listings to include fine tuning inspections
* v1.4.1 - Update SDSS-IV VI listings to get rid of few boss_novi entries
* v1.4.0 - Update SDSS-IV VI listings to include recent inspections
